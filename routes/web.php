<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@getUI');

Route::get('/home', 'HomeController@getUI');

//Mandar valores de sessión
Route::post('setSession', 'HomeController@setSession');

//Manda datos de login FB
Route::post('setUserData', 'HomeController@setUserData');

//Obtener variable de sessión vía get
Route::get('getSession', 'HomeController@getSession');

Route::get('/bar', 'BarController@getUI');

Route::get('/boletos', 'BoletosController@getUI');

Route::get('/dudas', 'DudasController@getUI');

Route::get('/leyendas', 'LeyendasController@getUI');

Route::get('/videos', 'VideosController@getUI');
Route::get('/videosleyendas', 'VideosController@getUI');

Route::get('/introduction', 'IntroductionController@getUI');

Route::get('/registro', function(){
  return view('registro');
});

// login cms

Route::resource('login','AdminLoginController');
Route::get('login', ['as' => 'loginx', 'uses' => 'AdminLoginController@login']);
Route::post('admin-auth', 'AdminLoginController@authenticate');
Route::post('admin-logout', ['as' => 'admin-logout', 'uses' => 'AdminLoginController@logout']);
Route::post('admin-get-user', ['as' => 'admin-get-user', 'uses' => 'AdminLoginController@get_user']);

Route::resource('admin','AdminController');
Route::get('admin', ['as' => 'admin', 'uses' => 'AdminController@index']);

Route::resource('feria','FeriaController');
Route::get('feria', ['as' => 'feria', 'uses' => 'FeriaController@index']);

// ++++ FERIAS ++++

Route::get('/f-home', function () {
    return view('ferias.home');
});

Route::get('/f-home2', function () {
    return view('ferias.f-home2');
});

Route::get('/landing', function () {
    return view('ferias.landing');
});

Route::get('/post', function () {
    return view('ferias.post');
});

Route::get('/sorteo', function () {
    return view('ferias.sorteo');
});

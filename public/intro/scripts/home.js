VICTORIA.home = (function () {
    var _config = {
        'scene_init': 1,                   //Para la experiencia completa colocar en 1

        'tolerance': 20,                   //Porcentaje de tolerancia en pantalla
        'pan_ratio': 3.5,                  //Movimiento horizontal en panografia Ideal de 3 a 4
        'value_click': 10,                 //Valor de cada click para llegar al 100,
        'timeDiscount': 500,               //Tiempo en milisegundos para descontar el clicker
        'scene_seconds': 9,                //Valor en segundos
        'time_elevator_down_per_floor': 2,  //Tiempo en segundos para elevador
        'time_for_red_lady': 3,             //Tiempo para que salga la dama de rojo al final en segundos


        'mop_time_to_drop': 4               //Tiempo en segundos para que el trapeador se caiga, DESACTIVADO POR NUEVA DINAMICA
    };

    var _$body,
        _$document,
        _document,
        _$window,

        _$heightmaxElements,

        _verticalScrollPosition,
        _$infiniteLoop,
        _$duplicate_content,
        _$duplicate_content_result,
        _$site_content,
        _$floors_divs,
        _$home_block,

        _$front_ui,
        _$progress_bar,

        _isMobile = false,

        _itsIn = false,
        _canPrepend = true,
        _$cancelInfinite,
        _infiniteCanceled = false,
        _$volume_recomendation,
        _$massive_click,
        _extraValueClicker = 0,
        _totalValueClicker = 0,
        _stopClicks = false,
        _timerDiscountClicker,
        _startTimer = false,

        _toleranceDistance = 0,

        _$scene3,

        // START SONIDOS

        _$mute_songs,

        _backgroundSong1,
        _backgroundSong2,
        _backgroundSong3,

        _dialog1,
        _dialog2,
        _dialog3,
        _dialog3Player = false,
        _dialog4,
        _dialog5,
        _dialog6,

        _screams,
        _screams_stop = true,
        _screamsTimeinterval,

        _last_dante_scream,
        _last_dama_Scream,

        _pasos_dante,
        _pasos_dama,

        _door_stopped,

        _fallingDown,
        _lightSounds,
        _lightSounds_2,
        _elevatorSounds,
        _elevator_doors,
        _elevator_doors_opened,
        _click_elevator,

        _blood_sound,

        _mopSound,
        _dingSound,

        _wind_sounds,
        _wind_sounds_2,

        _all_songs,
        _reverbFx_low,
        _reverbFx,
        _reverbFx_top,
        _grainFX,
        _grainFX_top,
        _grainFX_max,
        // END SONIDOS

        _mopDropped = false,
        _timerDrop,
        _horizontalValue,

        _$arm_left,
        _$arm_right,
        _$hand_clicker,

        isFallingDown = true,

        _$scene = document.getElementById('scene'),
        _$logo_parallax = document.getElementById('logo-parallax'),
        _particlesSpeed = 50,
        _particlesTotal = 20,

        _$home_horizontal,
        _$home_vertical,
        _$home_text,

        _$pano,
        _$panoWrapper,
        _$panoIMG,
        _constanScroll = 0,

        _$startTravel,

        _$scenes,
        _$scenes_grand,
        _$scene_backgrounds,
        _sceneInterval,

        _floor_height,
        _$elevator_numbers_li,

        _auxCheckpointElevator = false,
        _automaticFloorDownTimeInterval,
        _$elevator_exit,
        _$elevator_enter,
        _$elevator_cta,

        _$last_bg,
        _$last_elevator_numbers,
        _$block_elevator,

        _$single_hand,
        _$elevator_last_interaciton
        ;

    var _init = function _init(){
        _$body = $('body');
        _$body.scrollTop(0);

        _$front_ui = $(".front-ui");

        _$window = $(window);
        _$document = $(document);

        _toleranceDistance = _$window.height() / 100 * _config.tolerance;

        _$infiniteLoop = $('.infinite-loop');
        _$duplicate_content = $(".duplicate-content");
        _$cancelInfinite = $(".cancel-infinite");
        _$duplicate_content_result = _$duplicate_content.clone();

        _$startTravel = $(".startTravel");
        _$mute_songs = $(".mute-songs");
        _$volume_recomendation = $(".volume-recomendation");

        _$massive_click = $(".massive-click");

        _$home_text = $(".site .home-block .text");

        _$scenes_grand = $(".scenes");
        _$scenes = $(".scenes .scene");
        _$scene_backgrounds = $(".scene .backgrounds");

        _$home_vertical = $(".scroll-vertical");
        _$home_horizontal = $(".scroll-horizontal");

        _$elevator_numbers_li = $(".elevator-numbers ul li");

        _$elevator_cta = $(".elevator-cta");
        _$elevator_exit = $(".elevator-exit");
        _$elevator_enter = $(".elevator-enter");

        _$site_content = $(".site-content");
        _$floors_divs = $(".floors-divs");

        _$last_bg = $("#last-bg");
        _$last_elevator_numbers = $("#last-elevator-numbers");
        _$block_elevator = $(".block-elevator");

        _$single_hand = $("#single-hand");

        _$arm_left = $(".arm-left");
        _$arm_right = $(".arm-right");

        _$progress_bar = $(".front-ui .volume-recomendation .loading .loader-bar .padding .line");

        _$scene3 = $(".scene.scene-3");

        _$elevator_last_interaciton = $("#elevator-last-interaciton");
        _$home_block = $(".site .home-block");

        _$heightmaxElements = $("#specks, .site .home-block .scenes, .site .floors-divs section");
        _$heightmaxElements.height($(".site").height());

        _floor_height = $(".site").height();

        _$hand_clicker = $(".hand-clicker");

        // document.addEventListener("touchmove", _scrollEvent, false);
        // document.addEventListener("scroll", _scrollEvent, false);

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            _isMobile = true;
        }

        // if (window.DeviceOrientationEvent) {
        //     window.addEventListener("deviceorientation", function (yop) {
        //         _pan([event.beta, event.gamma]);
        //     }, true);
        // } else if (window.DeviceMotionEvent) {
        //     window.addEventListener('devicemotion', function () {
        //         _pan([event.acceleration.x * 2, event.acceleration.y * 2]);
        //     }, true);
        // } else {
        //     window.addEventListener("MozOrientation", function () {
        //         _pan([orientation.x * 50, orientation.y * 50]);
        //     }, true);
        // }


        var _parallax = new Parallax(_$scene);
        var _parallax2 = new Parallax(_$logo_parallax);

        if(_isMobile){
            _particlesSpeed = 10;
            _particlesTotal = 10;
        }
        _disableScroll();

        _$cancelInfinite.on('click', function(){
            _infiniteCanceled = true;
            $(this).addClass('active');
        });

        _backgroundSong1 = new Pizzicato.Sound({
            source: 'file',
            options: {
                path: 'sounds/background-1.mp3',
                loop: true
            }
        }, function () {
        });

        _backgroundSong2 = new Pizzicato.Sound({
            source: 'file',
            options: {
                path: 'sounds/background-2.mp3',
                loop: true
            }
        }, function () {
        });

        _backgroundSong3 = new Pizzicato.Sound({
            source: 'file',
            options: {
                path: 'sounds/background-3.mp3',
                loop: true
            }
        }, function () {
        });

        _dialog1 = new Pizzicato.Sound('sounds/voice-1.mp3', function() {});
        _dialog2 = new Pizzicato.Sound('sounds/voice-2.mp3', function() {});
        _dialog3 = new Pizzicato.Sound('sounds/voice-3.mp3', function() {});
        _dialog4 = new Pizzicato.Sound('sounds/voice-4.mp3', function() {});
        _dialog5 = new Pizzicato.Sound('sounds/voice-5.mp3', function() {});
        _dialog6 = new Pizzicato.Sound('sounds/voice-6.mp3', function() {});

        _pasos_dante = new Pizzicato.Sound('sounds/pasos-dante.mp3', function() {});
        _pasos_dama = new Pizzicato.Sound('sounds/pasos-dama.mp3', function() {});
        _last_dante_scream = new Pizzicato.Sound('sounds/last-dante-scream.mp3', function() {});
        _last_dama_Scream = new Pizzicato.Sound('sounds/last-scream.mp3', function() {});
        _blood_sound = new Pizzicato.Sound('sounds/blood.mp3', function() {});
        _elevator_doors = new Pizzicato.Sound('sounds/elevator-doors.mp3', function() {});
        _elevator_doors_opened = new Pizzicato.Sound('sounds/elevator-doors-opened.mp3', function() {});
        _door_stopped = new Pizzicato.Sound('sounds/puerta-detenida.mp3', function() {});

        _mopSound = new Pizzicato.Sound('sounds/mop.mp3', function() {});
        _dingSound = new Pizzicato.Sound('sounds/ding.mp3', function() {});
        _click_elevator = new Pizzicato.Sound('sounds/click-button.mp3', function() {});
        _fallingDown = new Pizzicato.Sound('sounds/caida.mp3', function() {});

        _screams = new Pizzicato.Sound({
            source: 'file',
            options: {
                path: 'sounds/gritos.mp3',
                loop: true
            }
        }, function () {
        });
        _wind_sounds = new Pizzicato.Sound({
            source: 'file',
            options: {
                path: 'sounds/wind.mp3',
                loop: true
            }
        }, function () {
        });
        _wind_sounds_2 = new Pizzicato.Sound({
            source: 'file',
            options: {
                path: 'sounds/wind-2.mp3',
                loop: true
            }
        }, function () {
        });
        _wind_sounds.attack = 0.5;
        _wind_sounds.release = 2;

        _lightSounds = new Pizzicato.Sound({
            source: 'file',
            options: {
                path: 'sounds/luces.mp3',
                loop: true
            }
        }, function () {
        });
        _lightSounds_2 = new Pizzicato.Sound({
            source: 'file',
            options: {
                path: 'sounds/luces-2.mp3',
                loop: true
            }
        }, function () {
        });
        _elevatorSounds = new Pizzicato.Sound({
            source: 'file',
            options: {
                path: 'sounds/elevador.mp3',
                loop: true
            }
        }, function () {
        });
        _reverbFx_low = new Pizzicato.Effects.Reverb({
            time: 0.3,
            decay: 0.3,
            reverse: false,
            mix: 0.7
        });
        _reverbFx = new Pizzicato.Effects.Reverb({
            time: 0.6,
            decay: 0.6,
            reverse: false,
            mix: 0.7
        });
        _reverbFx_top = new Pizzicato.Effects.Reverb({
            time: 0.9,
            decay: 0.9,
            reverse: false,
            mix: 0.8
        });

        _grainFX = new Pizzicato.Effects.Distortion({
            gain: 0.1
        });
        _grainFX_top = new Pizzicato.Effects.Distortion({
            gain: 0.2
        });
        _grainFX_max = new Pizzicato.Effects.Distortion({
            gain: 0.3
        });

        _mopSound.addEffect(_reverbFx);
        _click_elevator.addEffect(_reverbFx);
        _last_dama_Scream.addEffect(_reverbFx);
        _dialog6.addEffect(_reverbFx);
        _fallingDown.addEffect(_reverbFx);
        _last_dante_scream.addEffect(_reverbFx);


        _all_songs = [
            _backgroundSong1,
            _backgroundSong2,
            _backgroundSong3,
            _dialog1,
            _dialog2,
            _dialog3,
            _dialog4,
            _dialog5,
            _dialog6,
            _screams,
            _last_dante_scream,
            _last_dama_Scream,
            _pasos_dante,
            _pasos_dama,
            _door_stopped,
            _fallingDown,
            _lightSounds,
            _lightSounds_2,
            _elevatorSounds,
            _elevator_doors,
            _elevator_doors_opened,
            _click_elevator,
            _blood_sound,
            _mopSound,
            _dingSound,
            _wind_sounds,
            _wind_sounds_2
        ];

        _$mute_songs.on('click', function (event) {
            event.preventDefault();
            var $this = $(this);
            if($this.hasClass('active')){
                $this.removeClass('active');
                $.each(_all_songs, function (index, value) {
                   value.volume = 1;
                });
            }else{
                $this.addClass('active');
                $.each(_all_songs, function (index, value) {
                    value.volume = 0;
                });
            }
        });

        _$startTravel.on('click', function () {
            _initScenes();
            // _enableScroll();
        });

        for (var i = 1; i < _particlesTotal; i++) {
            twinkleLoop(i);
        }

        function twinkleLoop(i) {
            var duration = ((Math.random() * 5) + 3);
            duration = duration - ((495 - _particlesSpeed)/100);
            twinkle(i, duration);
            setTimeout(function() {
                twinkleLoop(i)
            }, duration * 1000);
        }

        function twinkle(id, duration) {
            var top = (Math.floor( Math.random() * 85)) + '%';
            var left = (Math.floor(Math.random() * 85)) + '%';

            $('#speck' + id).remove();
            $('#specks').append("<div class='speck' id='speck" + id + "'></div>");
            $('#speck' + id).css({
                'top': top,
                'left': left,
                'animation-duration': duration + 's',
                'animation-timing-function': 'cubic-bezier(0.250, 0.250, 0.750, 0.750)',
                'animation-name': 'twinkle'
            })
        }


        // _changeProgress(0);
        var randomTemporal;
        _$massive_click.on('click', function(event){
            event.preventDefault();
            _screams.play();
            setTimeout(function () {
                _lightSounds.play();
                _$scenes_grand.addClass('blinking-1');
                // _screams.removeEffect(_reverbFx);
                // _screams.addEffect(_grainFX_top);
                // _screams.addEffect(_reverbFx_top);
            }, 1000);
            if(_stopClicks == false){
                var $this = $(this);
                _totalValueClicker = _totalValueClicker + _config.value_click;
                _stopGlobalTimer();
                if(_totalValueClicker >= 100){
                    // EL PUZZLE PASÓ

                    if(isFallingDown){
                        isFallingDown = false;
                        _changeProgress(100);
                        _startTimer = true;
                        _screamsVolumenOut();
                        setTimeout(function () {
                            _$massive_click.removeClass('active');
                            setTimeout(function () {
                                _screams.addEffect(_grainFX);
                                _$single_hand.removeClass('active');
                                // _$block_elevator.removeClass('postition-right-2');
                                _startFallingDown();
                            }, 400);
                        }, 1000);
                    }
                }else{
                    _click_button();
                    _screams.volume  = 1;
                    _changeProgress(_totalValueClicker);
                    _startTimer = false;
                    randomTemporal = Math.floor(Math.random() * 3) + 1;
                    _$massive_click.addClass('move-' + randomTemporal);
                    setTimeout(function () {
                        _$massive_click.removeClass('move-1 move-2 move-3');
                    }, 100);
                }
            }
            if(_startTimer == false){
                _timerDiscountClicker = window.setInterval(function(){
                    _screamsVolumenOut();
                    _extraValueClicker -= 2;
                    _totalValueClicker = _totalValueClicker + _extraValueClicker;
                    if(_totalValueClicker <= 0){
                        _totalValueClicker = 0;
                        _stopGlobalTimer();
                        _changeProgress(_totalValueClicker);
                    }else{
                        _changeProgress(_totalValueClicker);
                    }
                }, _config.timeDiscount);
                _startTimer = true;
            }
        });

        var auxFloor;
        _document = document.documentElement;

        // window.onscroll = function() {
        _$floors_divs.on('scroll', function(){
            // _verticalScrollPosition = (window.pageYOffset || _document.scrollTop)  - (_document.clientTop || 0);
            _verticalScrollPosition = _$floors_divs.scrollTop();
            auxFloor = Math.floor(_verticalScrollPosition / _floor_height);
            if(_$home_vertical.hasClass('active')){
                // setTimeout(function(){
                //     _$home_vertical.removeClass('active');
                // }, 1500);
            }
            if(_verticalScrollPosition > (_floor_height * 4)  ){
                _disableScroll();
                _elevatorSounds.stop();
                _$home_vertical.removeClass('active');
                if(_auxCheckpointElevator == false){
                    _$elevator_numbers_li.removeClass('active');
                    _$elevator_numbers_li.eq(2).addClass('active');
                    _dingSound.play();
                    setTimeout(function(){
                        // _showScene(_config.scene_init);
                        _$scene3.addClass('little-blink');
                        setTimeout(function () {
                            _$scene3.addClass('alt doors-open');
                            _$scene3.removeClass('little-blink');
                            setTimeout(function () {
                                _$scene3.addClass('go-out');
                                setTimeout(function () {
                                    $("#dialog4").addClass('active');
                                    setTimeout(function () {
                                        _dialog4.play();
                                        setTimeout(function () {
                                            $("#dialog4").removeClass('active');
                                            setTimeout(function () {
                                                _$elevator_exit.addClass('active');
                                            }, 500);
                                        }, 4000);
                                    }, 500);
                                }, 300);
                            }, 2000)
                        }, 100);
                    }, 1000);
                }
                _auxCheckpointElevator = true;
            }else{
                if(_auxCheckpointElevator == false){
                    _$elevator_numbers_li.removeClass('active');
                    _$elevator_numbers_li.eq(6 - auxFloor).addClass('active');
                }else{
                    _$elevator_numbers_li.removeClass('active');
                    _$elevator_numbers_li.eq(2).addClass('active');
                }
            }
        });

        _$pano = $(".pano");
        _$panoWrapper = _$pano.find('.wrapper');
        _$panoIMG = _$panoWrapper.find('.bg');
        _constanScroll = ((_$panoIMG.width() - _$panoWrapper.width()) / 2) + 10;
        _$pano.scrollLeft(_constanScroll);
        $("#samePanoWidth").width(_$panoIMG.width() );

        document.getElementById('pano').addEventListener("touchmove", _panDrop, false);
        document.getElementById('pano').addEventListener("scroll", _panDrop, false);

        _$elevator_cta.on('click', function(){
            if($(this).hasClass('active')){
                _showScene(_config.scene_init);
                $(this).removeClass('active');
            }
        });
    };

    var _startFallingDown = function _startFallingDown() {
        _$scenes_grand.removeClass('blinking-1').addClass('blinking-2');
        setTimeout(function () {
            _dialog6.play();
            $("#dialog6").addClass('active');
            setTimeout(function () {
                _dialog6.addEffect(_grainFX_max);
                $("#dialog6").removeClass('active');
            }, 2500)
        }, 1000);

        setTimeout(function () {
            _backgroundSong2.stop();
            _elevatorSounds.stop();
            _lightSounds.stop();
            _screams.stop();
            _elevatorGetMad();
            $("#black-shadow-last").addClass('fade-in');
            _last_dante_scream.play();

            setTimeout(function () {
                _$arm_left.addClass('active');
                _$arm_right.addClass('active');
                setTimeout(function () {
                    _$arm_left.addClass('active-2');
                    _$arm_right.addClass('active-2');
                    setTimeout(function () {
                        _$arm_left.removeClass('active active-2');
                        _$arm_right.removeClass('active active-2');
                    }, 1500);
                }, 1500);
            }, 300);

            setTimeout(function () {
                $("#black-shadow-last").addClass('fade-out');
                setTimeout(function () {
                    _blood_sound.play();
                    $(".blood-splash").addClass('active');
                    setTimeout(function () {
                        $(".black-shadow").addClass('fade-in-2');
                        $(".blood-splash").addClass('byebye');
                        setTimeout(function () {
                            // START LAST SCENE
                            _showScene(_config.scene_init);
                            window.clearInterval(_sceneInterval);
                        }, 500);
                    }, 1000);
                    setTimeout(function () {
                        _$scenes_grand.removeClass('blinking-2');
                    }, 3000);
                }, 400);
            }, 3000);
        }, 7500);
    };

    var _elevatorGetMad = function _elevatorGetMad() {
        _fallingDown.play();

        $("#elevator-last-interaciton li.active").removeClass('active');
        $("#elevator-last-interaciton li:eq(9)").addClass('active');
        _automaticFloorDownTimeInterval = window.setInterval(function () {
            var temp = $("#elevator-last-interaciton").find('li.active').index();
            $("#elevator-last-interaciton").find('li.active').removeClass('active');
            if(temp > 0){
                $("#elevator-last-interaciton").find('li').eq(temp - 1).addClass('active');
            }else{
                $("#elevator-last-interaciton li").eq(( $("#elevator-last-interaciton li").length - 1 )).addClass('active');
            }
        }, _config.time_elevator_down_per_floor * 30);
        setTimeout(function () {
            window.clearInterval(_automaticFloorDownTimeInterval);
        }, 14000);
    };

    var _screamsVolumenOut = function _screamsVolumenOut() {
        // if(_screams_stop){
        //     _screams_stop = false;
        //     setTimeout(function () {
        //         var _screamsTimeinterval = window.setInterval(function () {
        //             console.log("Valor de volumen = " + _screams.volume);
        //             if(_screams.volume >= 0.1){
        //                 _screams.volume -= 0.1;
        //             }else{
        //                 _screams.volume = 0;
        //                 window.clearInterval(_screamsTimeinterval);
        //                 _screams_stop = true;
        //             }
        //         }, 300);
        //     }, 300);
        // }
        // window.clearInterval(_screamsTimeinterval);
    };

    var _panDrop = function _panDrop(){
        _horizontalValue = _$pano.scrollLeft();
        if(_$home_horizontal.hasClass('active')){
            _$home_horizontal.removeClass('active');
        }
    };

    var _stopGlobalTimer = function _stopGlobalTimer(){
        window.clearInterval(_timerDiscountClicker);
        _startTimer = false;
    };

    var _pan = function _pan(position){
        _$pano.scrollLeft( (_constanScroll + (position[1]  *  _config.pan_ratio) ) * -1 );
    };

    var _scrollEvent = function _scrollEvent(){
        if(_itsIn){
            if((_$document.scrollTop() - _toleranceDistance) < _$infiniteLoop.offset().top && _infiniteCanceled == false){
                _addInfiniteElement();
                _removeInfiniteElement();
            }
        }else{
            if(_$document.scrollTop() > _$infiniteLoop.offset().top){
                if(_canPrepend){
                    _addInfiniteElement();
                    _itsIn = true;
                }
            }
        }
    };

    var _changeProgress = function _changeProgress(value){
        var val = value;
        var $circle = $('#svg #bar');
        if (isNaN(val)) {
            val = 100;
        }
        else{
            var r = $circle.attr('r');
            var c = Math.PI*(r*2);
            if (val < 0) { val = 0;}
            if (val > 100) { val = 100;}
            var pct = ((100-val)/100)*c;
            $circle.css({ strokeDashoffset: pct});
            $('#cont').attr('data-pct',val);
        }
    };

    var _addInfiniteElement = function _addInfiniteElement(){
        _$duplicate_content.clone().insertBefore(".infinite-loop section:first");
        _canPrepend = false;
    };
    var _removeInfiniteElement = function _addInfiniteElement(){
        _$infiniteLoop.find(".duplicate-content:last").remove();
    };

    var keys = {37: 1, 38: 1, 39: 1, 40: 1};

    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }

    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }

    function preventMotion(event) {
        window.scrollTo(0, 0);
        event.preventDefault();
        event.stopPropagation();
    }

    var _disableScroll = function _disableScroll() {
        if (window.addEventListener) // older FF
            window.addEventListener('DOMMouseScroll', preventDefault, false);
        window.onwheel = preventDefault; // modern standard
        window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
        window.ontouchmove  = preventDefault; // mobile
        document.onkeydown  = preventDefaultForScrollKeys;

        window.addEventListener("scroll", preventMotion, false);
        window.addEventListener("touchmove", preventMotion, false);

        // _$body.addClass('block-scroll');
        _$floors_divs.addClass('block-scroll');
        _$home_block.removeClass('no-interact');
    };

    var _enableScroll = function _enableScroll() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null;
        window.onwheel = null;
        window.ontouchmove = null;
        document.onkeydown = null;

        window.removeEventListener("scroll", preventMotion, false);
        window.removeEventListener("touchmove", preventMotion, false);

        // _$body.removeClass('block-scroll');
        _$floors_divs.removeClass('block-scroll');
        _$home_block.addClass('no-interact');
    };

    var _preloadSoruces = function _preloadSoruces(){
        var queue = new createjs.LoadQueue();
        queue.installPlugin(createjs.Sound);
        queue.on("complete", handleComplete, this);
        queue.on("progress", handleProgress, this);

        // {id: "source-01-dama", src:"images/scenes/01-dama.png"},
        // {id: "source-01-dante", src:"images/scenes/01-dante.png"},
        // {id: "source-background-1", src:"images/scenes/background-1.jpg"},
        // {id: "source-04-dante", src:"images/scenes/04-dante.png"},
        // {id: "source-04-dama", src:"images/scenes/04-dama.png"},
        // {id: "source-last-dama-1", src:"images/scenes/last-dama-1.png"},
        // {id: "source-last-dama-2", src:"images/scenes/last-dama-2.png"},
        // {id: "source-last-dama-3", src:"images/scenes/last-dama-3.png"}

        queue.loadManifest([
            {id: "element-1", src:"images/scenes/01-dante.png"},
            {id: "element-2", src:"images/scenes/02-dante.png"},
            {id: "element-3", src:"images/scenes/02-door-2.png"},
            {id: "element-4", src:"images/scenes/02-door.png"},
            {id: "element-5", src:"images/scenes/02-hand.png"},
            {id: "element-6", src:"images/scenes/03-dama.png"},
            {id: "element-7", src:"images/scenes/03-dante.png"},
            {id: "element-8", src:"images/scenes/04-dama.png"},
            {id: "element-9", src:"images/scenes/04-dante.png"},
            {id: "element-10", src:"images/scenes/arm.png"},
            {id: "element-11", src:"images/scenes/background-1.jpg"},
            {id: "element-14", src:"images/scenes/background-4.jpg"},
            {id: "element-15", src:"images/scenes/background-5.jpg"},
            {id: "element-16", src:"images/scenes/background-6.jpg"},
            {id: "element-17", src:"images/scenes/blood.png"},
            {id: "element-18", src:"images/scenes/door-left-black.jpg"},
            {id: "element-19", src:"images/scenes/door-left.jpg"},
            {id: "element-20", src:"images/scenes/door-right-black.jpg"},
            {id: "element-21", src:"images/scenes/door-right.jpg"},
            {id: "element-22", src:"images/scenes/elevator-black.png"},
            {id: "element-23", src:"images/scenes/elevator-no-doors.png"},
            {id: "element-24", src:"images/scenes/hand-left.png"},
            {id: "element-25", src:"images/scenes/hand-right.png"},
            {id: "element-26", src:"images/scenes/last-dama-1.png"},
            {id: "element-27", src:"images/scenes/last-dama-2.png"},
            {id: "element-28", src:"images/scenes/last-dama-3.png"},
            {id: "element-29", src:"images/scenes/mob.png"},
            {id: "element-30", src:"images/scenes/pano.jpg"}
            // {id: "element-32", src:"images/scenes/vignette.png"}
        ]);

        function handleProgress(data){
            if( ( data.loaded.toFixed(2) * 100 ) < 10){}
            _$progress_bar.width(data.loaded.toFixed(2) * 100 + '%');
        }

        function handleComplete() {
            console.info("Precarga completada");
            setTimeout(function () {
                _$volume_recomendation.find('.loading').fadeOut(500);
                setTimeout(function(){
                    _wind_sounds.play();
                    _$volume_recomendation.find('.ready').hide().removeClass('hidden').fadeIn(500);
                }, 600);
            }, 100);
            // 3000
        }
    };

    var _showScene = function _showScene(number){
        console.log(number);
        if(number >= 7){
            window.clearInterval(_sceneInterval);
            return false;
        }
        _$floors_divs.removeClass('active');
        if(!_$floors_divs.hasClass('active')){
            setTimeout(function () {
                _$floors_divs.addClass('active');
            }, 2000);
        }
        if(number == 0){
            _$scenes.addClass('byebye');
        }else{
            if(number > 1){
                _wind_sounds.stop();
                _$scenes.eq(number - 2).addClass('byebye');
                setTimeout(function () {
                    _$scenes.removeClass('active');
                    _$scenes.eq(number - 1).addClass('active');
                },1000);
                // Un segundo de espera entre fades
            }else{
                _$scenes.removeClass('active');
                _$scenes.eq(number - 1).addClass('active');
            }
            if(number == 1){
                _pasos_dama.play();
                _backgroundSong1.play();
                _wind_sounds.volume = 0.9;
                setTimeout(function () {
                    _wind_sounds.volume = 0.5;
                    setTimeout(function () {
                        _wind_sounds.volume = 0.3;
                    }, 500);
                }, 500);
                setTimeout(function () {
                    _pasos_dante.play();
                }, 1500);
                setTimeout(function () {
                    _dialog1.play();
                }, 2500);
            }
            if(number == 2){
                setTimeout(function () {
                    _elevator_doors_opened.play();
                    setTimeout(function () {
                        _door_stopped.play();
                        _elevator_doors.stop();
                        setTimeout(function () {
                            _elevator_doors.play();
                        }, 300);
                    }, 2600);
                }, 1000);
            }
            if(number == 3){
                window.clearInterval(_sceneInterval);
                setTimeout(function(){
                    _$home_vertical.addClass('active');
                    _enableScroll();
                    _elevatorSounds.play();
                    $("#dialog3").addClass('active');
                    setTimeout(function () {
                        _dialog3.play();
                    }, 500);
                    setTimeout(function () {
                        $("#dialog3").removeClass('active');
                    }, 4000);
                }, 2500);
            }
            if(number == 4){
                _lightSounds_2.play();
                _wind_sounds_2.play();
                _backgroundSong1.stop();
                _backgroundSong2.play();


                setTimeout(function () {
                    $("#dialog5").addClass('active');
                    setTimeout(function () {
                        _dialog5.play();
                        setTimeout(function () {
                            $("#dialog5").removeClass('active');
                            setTimeout(function () {
                                _$home_horizontal.addClass('active');
                            }, 1000);
                        }, 4000);
                    }, 500);
                }, 1000);

                _enableScroll();

                // ESTA ES LA CONDICION PARA TIRAR EL TRAPEADOR
                // console.log($("#pano .wrapper").scrollLeft() + $(window).width());
                // console.log((_$panoIMG.width() / 2));
                var mobSeen = false;
                if(_mopDropped == false){
                    $("#pano .wrapper").scroll(function () {
                        if(mobSeen){
                            if($(this).scrollLeft() + $(window).width() < 1500){
                                if(_mopDropped == false){
                                    _dropTheMob();
                                }else{
                                    _mopDropped = true;
                                }
                            }
                        }else{
                            if($(this).scrollLeft() + $(window).width() > 1500){
                                mobSeen = true;
                            }
                        }
                    })
                }

                setTimeout(function () {
                    // if(_mopDropped == false && _horizontalValue <= (_$panoIMG.width() / 2)){
                    //     _dropTheMob();
                    // }else{
                    //     setTimeout(function () {
                    //         _dropTheMob();
                    //     }, 10000);
                    // }
                }, _config.mop_time_to_drop * 1000);
            }
            if(number == 5){
                window.clearInterval(_sceneInterval);
                _$front_ui.addClass('byebye');
                _backgroundSong2.stop();
                _backgroundSong3.play();
                _elevatorSounds.play();
                _lightSounds_2.stop();
                _wind_sounds_2.stop();

                _enableScroll();
                $("body").scrollTop(0);
                _disableScroll();


                // Tiempo para comenzar a bajar
                setTimeout(function () {
                    _screams.play();

                    _$floors_divs.addClass('animate-fake-up');
                    var section_height = _$floors_divs.find('section:eq(0)').height();
                    _$floors_divs.css({
                        '-webkit-transform': 'translateY('+ -(section_height * 2)+'px' +')',
                        '-moz-transform':    'translateY('+ -(section_height * 2)+'px' +')',
                        '-ms-transform':     'translateY('+ -(section_height * 2)+'px' +')',
                        '-o-transform':      'translateY('+ -(section_height * 2)+'px' +')',
                        'transform':         'translateY('+ -(section_height * 2)+'px' +')',
                        'height': section_height * 2 + 'px'
                    });


                    _$elevator_last_interaciton.find('li').removeClass('active');
                    _$elevator_last_interaciton.find('li:eq(2)').addClass('active');
                    setTimeout(function () {
                        _$elevator_last_interaciton.find('li').removeClass('active');
                        _$elevator_last_interaciton.find('li:eq(1)').addClass('active');
                        _screams.addEffect(_reverbFx_low);
                        setTimeout(function () {
                            _dingSound.play();
                            _$elevator_last_interaciton.find('li').removeClass('active');
                            _$elevator_last_interaciton.find('li:eq(0)').addClass('active');
                            _screams.removeEffect(_reverbFx_low);
                            _screams.addEffect(_reverbFx);
                            // _screams.addEffect(_grainFX);
                            setTimeout(function () {
                                _$front_ui.removeClass('byebye');
                                setTimeout(function () {
                                    _$massive_click.addClass('active');
                                    _$hand_clicker.addClass('active')
                                }, 400);
                            }, 2000);
                        }, 3000);
                    }, 4000);
                }, 2000);
            }
            if(number == 6){
                window.clearInterval(_sceneInterval);
                setTimeout(function () {
                    _dingSound.play();
                    setTimeout(function () {
                        _elevator_doors.play();
                        $(".scene-6").addClass('open');
                        _backgroundSong2.stop();
                        _backgroundSong3.stop();
                        _backgroundSong1.play();

                        setTimeout(function () {
                            $(".scene.scene-6 .cuenta-la-leyenda").addClass('active');
                            setTimeout(function () {
                                _last_dama_Scream.play();
                                $(".last-dama-1").addClass('active');
                                setTimeout(function () {
                                    $(".scene.scene-6 .cuenta-la-leyenda").addClass('byebye');
                                    $(".last-dama-1").removeClass('active');
                                    $(".last-dama-2").addClass('active');
                                    setTimeout(function () {
                                        $(".last-dama-2").removeClass('active');
                                        $(".last-dama-3").addClass('active');
                                        setTimeout(function () {
                                            $(".last-dama-3").removeClass('active');
                                            setTimeout(function () {
                                                $("#last-message").addClass('active');
                                            }, 1000);
                                        }, 400);
                                    }, 400);
                                }, 400);
                            }, _config.time_for_red_lady * 1000);
                        }, 2500);
                    }, 2500);
                }, 2000);

                // LETRAS NEON
                var textHolder = document.getElementById('flashing'),
                    text = textHolder.innerHTML,
                    chars = text.length,
                    newText = '',
                    i;

                for (i = 0; i < chars; i += 1) {
                    newText += '<i>' + text.charAt(i) + '</i>';
                }

                textHolder.innerHTML = newText;
                var letters = document.getElementsByTagName('i'),
                    flickers = [5, 7, 9, 11, 13, 15, 17],
                    randomLetter,
                    flickerNumber,
                    counter;

                function randomFromInterval(from,to) {
                    return Math.floor(Math.random()*(to-from+1)+from);
                }
                function hasClass(element, cls) {
                    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
                }
                function flicker() {
                    counter += 1;
                    if (counter === flickerNumber) {
                        return;
                    }
                    setTimeout(function () {
                        if(hasClass(randomLetter, 'off')) {
                            randomLetter.className = '';
                        }
                        else {
                            randomLetter.className = 'off';
                        }
                        flicker();
                    }, 30);
                }
                (function loop() {
                    var rand = randomFromInterval(500,3000);
                    randomLetter = randomFromInterval(0, 3);
                    randomLetter = letters[randomLetter];
                    flickerNumber = randomFromInterval(0, 6);
                    flickerNumber = flickers[flickerNumber];
                    setTimeout(function() {
                        counter = 0;
                        flicker();
                        loop();
                    }, rand);
                }());
            }

            if(number != 1 && number != 3 && number != 4){
                setTimeout(function () {
                    if (number == 2) _dialog2.play();
                }, 6000);
            }
        }
        _config.scene_init ++;
    };

    var _click_button = function _click_button() {
        _$single_hand.addClass('click');
        setTimeout(function () {
            _click_elevator.play();
            _$single_hand.removeClass('click');
        },300);
    };

    var _dropTheMob = function _dropTheMob() {
        if(_mopDropped == false){
            $("#dropThisMob").addClass('pre-active');
            // setTimeout(function () {
            //     _disableScroll();
            //     $("#pano .wrapper").animate({scrollLeft: 1350}, 500, 'swing');
            // }, 4700);
            setTimeout(function () {
                $("#dropThisMob").addClass('active');
                _$home_horizontal.removeClass('active');
                if(_mopDropped == false){

                    setTimeout(function () {
                        _mopSound.play();
                        // if(_mopDropped == false){
                        //
                        // }
                    }, 300);
                    setTimeout(function () {
                        _$elevator_enter.addClass('active');
                        // if(_mopDropped == false){
                        // }
                    }, 4000);
                    _mopDropped = true;
                }
            }, 5000);
        }
    };

    var _initScenes = function _initScenes() {
        _$volume_recomendation.addClass('byebye');
        $('#scene').addClass('byebye');
        _$home_text.addClass('byebye');
        setTimeout(function(){
            if(_config.scene_init == 1){
                _showScene(_config.scene_init);
                setTimeout(function () {
                    _showScene(_config.scene_init);
                    _sceneInterval = window.setInterval(function () {
                        _showScene(_config.scene_init);
                    }, _config.scene_seconds * 1000);
                }, 5000);
            }else{
                _showScene(_config.scene_init);
                if(_config.scene_init < 3){
                    _sceneInterval = window.setInterval(function () {
                        _showScene(_config.scene_init);
                    }, _config.scene_seconds * 1000);
                }
            }
        }, 2000);
    };

    return {
        init: function init(){
            _init();
            _preloadSoruces();
        },

        // Funciones para debuggear
        updateProgress: function updateProgress(value) {
            _changeProgress(value);
        },
        getMad: function getMad() {
            // _elevatorGetMad();
            _startFallingDown();
        }
    }
})();

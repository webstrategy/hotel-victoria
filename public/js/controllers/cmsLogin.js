$(document).ready(function(){

  $('form').submit(function(event) {

    event.preventDefault();

    $.post(base_url + 'login/validate',$('form').serialize(),'json')
      .done(function(data) {
        // console.log('response = '+ data);
        if($.parseJSON(data).status == 200 ){
          window.location.replace(base_url + 'history');
        } else {
          $('.dialog-msg').text($.parseJSON(data).msg);
          $('#form-modal').modal({
            backdrop: true,
            keyboard: false,
            focus:true
          });
          $('#form-modal').modal('show');
        }
      }).fail(function() {

        $('.dialog-msg').text('Ocurrió un error, vuelve a intentarlo !');
        $('#form-modal').modal({
          backdrop: false,
          keyboard: false,
          focus:true
        });
        $('#form-modal').modal('show');
      });
  });

});

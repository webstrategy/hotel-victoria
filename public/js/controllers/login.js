ga('set', 'contentGroup1', 'El Hotel');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function(){

  var daysInput = $('#day_field');
  var monthsInput = $('#month_field');
  var yearInput = $('#year_field');

  filterKeys();

  $.get('getSession').done(function(res){
    res = $.parseJSON(res);
    if(res.success) {
      if(!res.response) {
        $('#loginModal').modal({
                backdrop: 'static',
                keyboard: false,
                focus:true
              });
        $('#loginModal').modal('show');
      }
    }
  });

  $('#login-afirmative').click(function(){
    $.post('setSession',{ allow : true }).done(function(res){
      res = $.parseJSON(res);
      if(res.status == 200)
        $('#loginModal').modal('hide');

    });
  });

  $('#login-negative').click(function(){
    $('.dialog-msg').text('Debes ser mayor de edad, para poder acceder al sitio.');
    $('#alert-modal').modal({
            backdrop: true,
            keyboard: false,
            focus:true
          });
    $('#alert-modal').modal('show');
  });

  $('#fb-login').click(function(){
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
  });

  $('#login_btn').click(function(){
    ageValidation(yearInput.val(),monthsInput.val(),daysInput.val());
  });

  $('video').each(function(){

      if ($(this).css('display') == 'block') {
          // $(this).attr('loop','loop');
          $(this)[0].play();
      } else {
          $(this)[0].pause();
      }
  });

});

function filterKeys() {
  $('#day_field').keypress(function(key) {
       if(key.charCode < 48 || key.charCode > 57) {
         if(key.charCode != 0) return false;
       }
   });

   $('#month_field').keypress(function(key) {
        if(key.charCode < 48 || key.charCode > 57) {
          if(key.charCode != 0) return false;
        }
    });

    $('#year_field').keypress(function(key) {
         if(key.charCode < 48 || key.charCode > 57) {
           if(key.charCode != 0) return false;
         }
     });
}

function ageValidation(year,month,days) {
  var today = new Date();
  var yearToMilis = 1000 * 60 * 60 * 24 * 365;
  var allowedYears = 18;
  var allowedMilis = yearToMilis * allowedYears;
  var sessionFlag = 'unauthorized';
  var userBirthDate = null;
  var actualYear = today.getFullYear();


  if(month > 12 || days > 31 || year > actualYear || year.length < 4) {
    showDialog('Fecha inválida, favor de ingresar una fecha válida.');
    return;
  }

  month = month - 1;


  if(year != null && month != null && days != null) {
    if(days <= daysInMonth(month,year)) {
      userBirthDate = new Date(year,month,days);
      if((today.getTime() - userBirthDate.getTime()) > allowedMilis){
        //Es mayor de edad
        $.post('setSession',{ allow : true }).done(function(res){
          console.log(res);
          ga('send', {
                hitType: 'event',
                eventCategory: 'Login',
                eventAction: 'click',
                eventLabel: 'Validación de edad'
              });

              if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                window.location = 'home';
              } else {
                 window.location = 'introduction';
              }

        }).fail(function(res){
          showDialog(res);
        });
      } else {
        //Es menor de edad
        showDialog('Debes ser mayor de edad, para poder acceder al sitio.');
      }
    } else {
      showDialog('De acuerdo al mes y año especificados, sólo se tienen ' + daysInMonth(month,year) + ' días. Favor de corregir el día.');
    }
  }

  console.log('date = ' + year + month + days);
  console.log('user birthdate = ' + userBirthDate);
  console.log(daysInMonth(month,year));

}

function showDialog(msg) {
  $('.dialog-msg').text(msg);
  $('#alert-modal').modal({
          backdrop: true,
          keyboard: false,
          focus: false
        });
  $('#alert-modal').modal('show');
}

function daysInMonth(month,year) {
  var dd = new Date(year, month + 1, 0);
  return dd.getDate();
}

function statusChangeCallback(response) {
  console.log(response);
  // The response object is returned with a status field that lets the
  // app know the current login status of the person.
  // Full docs on the response object can be found in the documentation
  // for FB.getLoginStatus().
  if (response.status === 'connected') {
    $.post('setSession',{ allow : true }).done(function(res){
      res = $.parseJSON(res);
      if(res.success) {
        ga('send', {
              hitType: 'event',
              eventCategory: 'Login',
              eventAction: 'click',
              eventLabel: 'Facebook login'
            });
        $('#loginModal').modal('hide');
        if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
          window.location = 'home';
        } else {
           window.location = 'introduction';
        }
      }
    });
    // Logged into your app and Facebook.
    // testAPI();
  } else {
    // The person is not logged into your app or we are unable to tell.
    FB.login(function(response){
      console.log(response);
      if(response.status === 'connected') {
        FB.api('/me?fields=id,first_name,last_name,name,email,birthday,gender,locale,age_range', function(response) {
            console.log(response);
            $.post('setUserData',
              {
                name: response.name,
                first_name: response.first_name,
                last_name: response.last_name,
                email: response.email,
                gender: response.gender,
                age_range: response.age_range.min,
                id: response.id
               }
           ).done(function(res){
             res = $.parseJSON(res);
             if(res.success)
             $.post('setSession',{ allow : true }).done(function(res){
               res = $.parseJSON(res);
               if(res.success) {
                 ga('send', {
                       hitType: 'event',
                       eventCategory: 'Login',
                       eventAction: 'click',
                       eventLabel: 'Facebook login'
                     });
                 $('#loginModal').modal('hide');
                 if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                   window.location = 'home';
                 } else {
                    window.location = 'introduction';
                 }
               }
             });
           });
        });
      }
      // Handle the response object, like in statusChangeCallback() in our demo
      // code.
    },{scope: 'public_profile,email'});
  }
}

ga('set', 'contentGroup2', 'Compra boletos');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function(){
    var show_dates = false;
    var show_hours = false;
    var show_dates_m = false;
    var show_hours_m = false;
    var selectedDate;

    var events_cal = new Array();


    ticketsAll();

    //Set calendar funtion
    // $(function() {
      $("#fecha").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        minDate: new Date(2017, 10, 20),
        maxDate: new Date(2017, 11, 30),
        beforeShowDay: function(date) {
            var result = [false, '', null];
            var matching = $.grep(events_cal, function(event) {
                return event.date.valueOf() === date.valueOf();
            });

            if (matching.length) {
                result = [true, 'highlight', null];
            }
            return result;
        },
        onSelect: function(dateText) {
            var dateText0 = dateText.split('-').join('/');
            var dateText1 = dateText0.split('/');
            dateText2 = dateText1[1]+'/'+dateText1[0]+'/'+dateText1[2];

            var date;
            var selectedDate = new Date(dateText2);
            var i = 0;
            var event = null;

            while (i < events_cal.length && !event) {
                date = events_cal[i].date;

                if (selectedDate.valueOf() === date.valueOf()) {
                    event = events_cal[i];
                }
                i++;
            }
            if (event) {
                eventsFilterArray = new Array();
                eventsFilterArray.push(event);
                sendGoogleTagEvent('Menú - Filtro fecha', dateText);
                updateUI(eventsFilterArray);
            }
        }
      });

      $("#fecha-m").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        minDate: new Date(2017, 10, 20),
        maxDate: new Date(2017, 11, 30),
        beforeShowDay: function(date) {
            var result = [false, '', null];
            var matching = $.grep(events_cal, function(event) {
                return event.date.valueOf() === date.valueOf();
            });

            if (matching.length) {
                result = [true, 'highlight', null];
            }
            return result;
        },
        onSelect: function(dateText) {
            var dateText0 = dateText.split('-').join('/');
            var dateText1 = dateText0.split('/');
            dateText2 = dateText1[1]+'/'+dateText1[0]+'/'+dateText1[2];

            var date;
            var selectedDate = new Date(dateText2);
            var i = 0;
            var event = null;

            while (i < events_cal.length && !event) {
                date = events_cal[i].date;

                if (selectedDate.valueOf() === date.valueOf()) {
                    event = events_cal[i];
                }
                i++;
            }
            if (event) {
                eventsFilterArray = new Array();
                eventsFilterArray.push(event);
                sendGoogleTagEvent('Menú - Filtro fecha', dateText);
                updateUI(eventsFilterArray);
            }
        }
      });

    // });

    function ticketsAll() {

      $.getJSON('json/events.json',function(events){
        var html = '';
        var html_dropdown_dates = '';
        var html_dropdown_dates_m = '';
        var html_dropdown_hours = '';
        var hours = new Array();
        var fillHours = [];
        var days = new Array();
        var fillDays = [];
        var eventsArray = new Array();
        var eventsFilterArray = new Array();
        var ticket = {
          date: '',
          day: '',
          month: '',
          type: '',
          init: '',
          uri: ''
        };

        $.each(events.events,function(i,item) {
          item.date = item.date.split('-').join('/');

          var date = new Date(item.date);
          var month = ["Ene", "Feb", "Mar", "Abr", "May", "Jun","Jul", "Ago", "Sep", "Oct", "Nov", "Dic"][date.getMonth()];
          var year = date.getFullYear();
          var day = date.getDate();
          var clase;

          if(item.available==0){
              clase = 'boton_agotado';
              textobtn = "BOLETOS AGOTADOS";
          }

          if(item.available==1){
            clase = 'boton_boletos';
            textobtn = "COMPRA TUS BOLETOS";

            html += '<div class="row" style="padding-top:20px">'
                     + '<div class="col-md-offset-1 col-md-1 col-sm-1 col-xs-3" align="center">'
                       +  '<div class="caja_boletos">'
                         +  '<div class="txt_boletos_tl">' + day + '</div>'
                         +  '<div class="txt_boletos_til">' + month + '</div>'
                      +   '</div>'
                     +  '</div>'
                    + '<div class="col-md-5 col-sm-7 col-xs-12">'
                    +    '<div class="caja_boletos2">'
                    +      '<div class="txt_boletos_til2"><img src="'+(item.type == 'General' ? 'images/hv_skull_n.png' : 'images/hv_fire_n.png')+'" alt="">&nbsp;' + (item.type == 'General' ? 'Hotel de Leyendas Victoria ' : 'Hotel de Leyendas Victoria - Acceso Total ') +  '</div>'
                    +     '<div class="txt_boletos">Paseo de la Reforma 109, Col. Tabacalera, Ciudad de México, DF 06030 Ciudad de México, CDMX Recuerda estar 20 minutos antes del evento</div>'
                    +    '</div>'
                    +  '</div>'
                    + '<div class="col-md-4 col-sm-4 col-xs-12 compra-boletos">'
                    +  '<button class="'+clase+'" onclick="launchUrl(\'' + item.url + '\',\'' + 'Botón - Compra tus boletos' + '\',\'' + day + ' ' + month  + ', ' + (item.type == 'General' ? 'Hotel de Leyendas Victoria ' : 'Hotel de Leyendas Victoria - Acceso Total ') + item.init + ' hrs.' + '\');" id="boton-ticket-url">' + textobtn + '</button>'
                    + '</div>'
                    + '</div>'
                    + '<div class="row" style="margin-top:30px">'
                    +  '<div class="col-md-offset-1 col-md-10 separador">&nbsp;</div>'
                    + '</div>';

            hours.push(item.init);
            days.push(item.date);

            ticket = new Object();

            ticket.date = item.date;
            ticket.day = day;
            ticket.month = month;
            ticket.type = item.type;
            ticket.init = item.init;
            ticket.url = item.url;

            eventsArray.push(ticket);
            events_cal.push({
              date: new Date(item.date),
              day: day,
              month: month,
              type: item.type,
              init: item.init,
              url: item.url,
              available: item.available
            });
          }

        });


        $.each(days, function(i, day){
            if($.inArray(day, fillDays) === -1)
              fillDays.push(day);
        });

        fillDays.sort();


        $.each(fillDays, function(i, day){
          day = day.split('-').join('/');

          var date = new Date(day);
          var month = ["Ene", "Feb", "Mar", "Abr", "May", "Jun","Jul", "Ago", "Sep", "Oct", "Nov", "Dic"][date.getMonth()];
          var dayOfMonth = date.getDate();

          html_dropdown_dates += '<a id="dropdown' + i + '">' + dayOfMonth + ' ' + month  + '</a>';
          html_dropdown_dates_m += '<a id="dropdown_m' + i + '">' + dayOfMonth + ' ' + month  + '</a>';

        });

          $('#tickets-area').append(html);
          $('#dropdown-dates').append(html_dropdown_dates);
          $('#dropdown-dates-m').append(html_dropdown_dates_m);

          $.each(fillDays, function(i, date) {
            $('#dropdown' + i).click(function(){
              sendGoogleTagEvent('Menú - Filtro fecha', $('#dropdown' + i).text());
              var selectedDate = fillDays[i];
              eventsFilterArray = new Array();
              $.each(eventsArray, function(i,eventObject){
                if(selectedDate === eventObject.date)
                  eventsFilterArray.push(eventObject);
              });
              updateUI(eventsFilterArray);
            });

            $('#dropdown_m' + i).click(function(){
              sendGoogleTagEvent('Menú - Filtro fecha', $('#dropdown_m' + i).text());
              var selectedDate = fillDays[i];
              eventsFilterArray = new Array();
              $.each(eventsArray, function(i,eventObject){
                if(selectedDate === eventObject.date)
                  eventsFilterArray.push(eventObject);
              });
              updateUI(eventsFilterArray);
            });

          });


      });

    }

    $('#dropdown-item-dates').click(function(e){
      e.stopPropagation();

      $('#dropdown-hours').css('display','none');

      if(show_dates) {
        $('#dropdown-dates').css('display','none');
        show_dates = false;
      } else {
        $('#dropdown-dates').css('display','block');
        show_dates = true;
      }

    });

    function ticketsMortal(e) {
      var myjson;

      var html_dropdown_dates = '';
      var html_dropdown_dates_m = '';
      var html_dropdown_hours = '';
      var hours = new Array();
      var fillHours = [];
      var days = new Array();
      var fillDays = [];
      var eventsArray = new Array();
      var eventsFilterArray = new Array();
      var ticket = {
        date: '',
        day: '',
        month: '',
        type: '',
        init: '',
        uri: ''
      };

      $.getJSON('json/events.json', function(json) {
        myjson = json;

        var html = "";
        $.each(json.events,function(i,item) {
          item.date = item.date.split('-').join('/');

          var date = new Date(item.date);
          var month = ["Ene", "Feb", "Mar", "Abr", "May", "Jun","Jul", "Ago", "Sep", "Oct", "Nov", "Dic"][date.getMonth()];
          var year = date.getFullYear();
          var day = date.getDate();

          if (item.type == 'General') {
              var clase;
              if(item.available==0){
                  clase = 'boton_agotado';
                  textobtn = "BOLETOS AGOTADOS";
              }

              if(item.available==1){
                clase = 'boton_boletos';
                textobtn = "COMPRA TUS BOLETOS";

                html += '<div class="row" style="padding-top:20px">'
                         + '<div class="col-md-offset-1 col-md-1 col-sm-1 col-xs-3" align="center">'
                           +  '<div class="caja_boletos">'
                             +  '<div class="txt_boletos_tl">' + day + '</div>'
                             +  '<div class="txt_boletos_til">' + month + '</div>'
                          +   '</div>'
                         +  '</div>'
                        + '<div class="col-md-5 col-sm-7 col-xs-12">'
                        +    '<div class="caja_boletos2">'
                        +      '<div class="txt_boletos_til2"><img src="'+(item.type == 'General' ? 'images/hv_skull_n.png' : 'images/hv_fire_n.png')+'" alt="">&nbsp;' + (item.type == 'General' ? 'Hotel de Leyendas Victoria ' : 'Hotel de Leyendas Victoria - Acceso Total ') +  '</div>'
                        +     '<div class="txt_boletos">Paseo de la Reforma 109, Col. Tabacalera, Ciudad de México, DF 06030 Ciudad de México, CDMX Recuerda estar 20 minutos antes del evento</div>'
                        +    '</div>'
                        +  '</div>'
                        + '<div class="col-md-4 col-sm-4 col-xs-12 compra-boletos">'
                        +  '<button class="'+clase+'" onclick="launchUrl(\'' + item.url + '\',\'' + 'Botón - Compra tus boletos' + '\',\'' + day + ' ' + month  + ', Hotel de Leyendas Victoria' + item.init + ' hrs.' + '\');" id="boton-ticket-url">'+textobtn+'</button>'
                        + '</div>'
                        + '</div>'
                        + '<div class="row" style="margin-top:30px">'
                        +  '<div class="col-md-offset-1 col-md-10 separador">&nbsp;</div>'
                        + '</div>';

                hours.push(item.init);
                days.push(item.date);

                ticket = new Object();

                ticket.date = item.date;
                ticket.day = day;
                ticket.month = month;
                ticket.type = item.type;
                ticket.init = item.init;
                ticket.url = item.url;

                eventsArray.push(ticket);

                events_cal.push({
                  date: new Date(item.date),
                  day: day,
                  month: month,
                  type: item.type,
                  init: item.init,
                  url: item.url,
                  available: item.available
                });

              }

          }


        });

        $('#tickets-area').html(html);
        $('#dropdown-dates').html('&nbsp;');
        $('#dropdown-dates-m').html('&nbsp;');


      $.each(days, function(i, day){
          if($.inArray(day, fillDays) === -1)
            fillDays.push(day);
      });

      fillDays.sort();


      $.each(fillDays, function(i, day){
        day = day.split('-').join('/');

        var date = new Date(day);
        var month = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"][date.getMonth()];
        var dayOfMonth = date.getDate();

        html_dropdown_dates += '<a id="dropdown' + i + '">' + dayOfMonth + ' ' + month + '</a>';
        html_dropdown_dates_m += '<a id="dropdown_m' + i + '">' + dayOfMonth + ' ' +  month  + '</a>';

      });

        $('#dropdown-dates').html(html_dropdown_dates);
        $('#dropdown-dates-m').html(html_dropdown_dates_m);


        $.each(fillDays, function(i, date) {
          $('#dropdown' + i).click(function(){
            sendGoogleTagEvent('Menú - Filtro fecha', $('#dropdown' + i).text());
            var selectedDate = fillDays[i];
            eventsFilterArray = new Array();
            $.each(eventsArray, function(i,eventObject){
              if(selectedDate === eventObject.date)
                eventsFilterArray.push(eventObject);
            });
            updateUI(eventsFilterArray);
          });

          $('#dropdown_m' + i).click(function(){
            sendGoogleTagEvent('Menú - Filtro fecha', $('#dropdown_m' + i).text());
            var selectedDate = fillDays[i];
            eventsFilterArray = new Array();
            $.each(eventsArray, function(i,eventObject){
              if(selectedDate === eventObject.date)
                eventsFilterArray.push(eventObject);
            });
            updateUI(eventsFilterArray);
          });

        });

      });

    }

    function ticketsExtrema(e) {
      var myjson;
      $.getJSON('json/events.json', function(json) {
        myjson = json;

        var html_dropdown_dates = '';
        var html_dropdown_dates_m = '';
        var html_dropdown_hours = '';
        var hours = new Array();
        var fillHours = [];
        var days = new Array();
        var fillDays = [];
        var eventsArray = new Array();
        var eventsFilterArray = new Array();
        var ticket = {
          date: '',
          day: '',
          month: '',
          type: '',
          init: '',
          uri: ''
        };

        var html = "";
        $.each(json.events,function(i,item) {
          item.date = item.date.split('-').join('/');

          var date = new Date(item.date);
          var month = ["Ene", "Feb", "Mar", "Abr", "May", "Jun","Jul", "Ago", "Sep", "Oct", "Nov", "Dic"][date.getMonth()];
          var year = date.getFullYear();
          var day = date.getDate();

          if (item.type == 'Extrema') {
              var clase;
              if(item.available==0){
                  clase = 'boton_agotado';
                  textobtn = "BOLETOS AGOTADOS";
              }

              if(item.available==1){
                clase = 'boton_boletos';
                textobtn = "COMPRA TUS BOLETOS";

                html += '<div class="row" style="padding-top:20px">'
                         + '<div class="col-md-offset-1 col-md-1 col-sm-1 col-xs-3" align="center">'
                           +  '<div class="caja_boletos">'
                             +  '<div class="txt_boletos_tl">' + day + '</div>'
                             +  '<div class="txt_boletos_til">' + month + '</div>'
                          +   '</div>'
                         +  '</div>'
                        + '<div class="col-md-5 col-sm-7 col-xs-12">'
                        +    '<div class="caja_boletos2">'
                        +      '<div class="txt_boletos_til2"><img src="'+(item.type == 'General' ? 'images/hv_skull_n.png' : 'images/hv_fire_n.png')+'" alt="">&nbsp;' + (item.type == 'General' ? 'Hotel de Leyendas Victoria ' : 'Hotel de Leyendas Victoria - Acceso Total ') +  '</div>'
                        +     '<div class="txt_boletos">Paseo de la Reforma 109, Col. Tabacalera, Ciudad de México, DF 06030 Ciudad de México, CDMX Recuerda estar 20 minutos antes del evento</div>'
                        +    '</div>'
                        +  '</div>'
                        + '<div class="col-md-4 col-sm-4 col-xs-12 compra-boletos">'
                        +  '<button class="'+clase+'" onclick="launchUrl(\'' + item.url + '\',\'' + 'Botón - Compra tus boletos' + '\',\'' + day + ' ' + month  + ', Hotel de Leyendas Victoria - Acceso Total' + item.init + ' hrs.' + '\');" id="boton-ticket-url">'+textobtn+'</button>'
                        + '</div>'
                        + '</div>'
                        + '<div class="row" style="margin-top:30px">'
                        +  '<div class="col-md-offset-1 col-md-10 separador">&nbsp;</div>'
                        + '</div>';

                        hours.push(item.init);
                        days.push(item.date);

                        ticket = new Object();

                        ticket.date = item.date;
                        ticket.day = day;
                        ticket.month = month;
                        ticket.type = item.type;
                        ticket.init = item.init;
                        ticket.url = item.url;

                        eventsArray.push(ticket);

                        events_cal.push({
                          date: new Date(item.date),
                          day: day,
                          month: month,
                          type: item.type,
                          init: item.init,
                          url: item.url,
                          available: item.available
                        });

              }

          }


        });

        $('#tickets-area').html(html);

        $.each(days, function(i, day){
            if($.inArray(day, fillDays) === -1)
              fillDays.push(day);
        });

        fillDays.sort();


        $.each(fillDays, function(i, day){
          day = day.split('-').join('/');

          var date = new Date(day);
          var month = ["Ene", "Feb", "Mar", "Abr", "May", "Jun","Jul", "Ago", "Sep", "Oct", "Nov", "Dic"][date.getMonth()];
          var dayOfMonth = date.getDate();

          html_dropdown_dates += '<a id="dropdown' + i + '">' + dayOfMonth + ' ' + month  + '</a>';
          html_dropdown_dates_m += '<a id="dropdown_m' + i + '">' + dayOfMonth + ' ' + month  + '</a>';

        });

        $('#dropdown-dates').html(html_dropdown_dates);
        $('#dropdown-dates-m').html(html_dropdown_dates_m);


          $.each(fillDays, function(i, date) {
            $('#dropdown' + i).click(function(){
              sendGoogleTagEvent('Menú - Filtro fecha', $('#dropdown' + i).text());
              var selectedDate = fillDays[i];
              eventsFilterArray = new Array();
              $.each(eventsArray, function(i,eventObject){
                if(selectedDate === eventObject.date)
                  eventsFilterArray.push(eventObject);
              });
              updateUI(eventsFilterArray);
            });

            $('#dropdown_m' + i).click(function(){
              sendGoogleTagEvent('Menú - Filtro fecha', $('#dropdown_m' + i).text());
              var selectedDate = fillDays[i];
              eventsFilterArray = new Array();
              $.each(eventsArray, function(i,eventObject){
                if(selectedDate === eventObject.date)
                  eventsFilterArray.push(eventObject);
              });
              updateUI(eventsFilterArray);
            });

          });
      });
    }

    $('#ex-mortal').click(function(e){
      ticketsMortal(e);
    });

    // $('#ex-extrema').click(function(e){
    //   ticketsExtrema(e);
    // });

    $('#ex-mortal-m').click(function(e){
      ticketsMortal(e);
    });

    // $('#ex-extrema-m').click(function(e){
    //   ticketsExtrema(e);
    // });

    $('#dropdown-item-hours').click(function(e){
      e.stopPropagation();

      $('#dropdown-dates').css('display','none');

      if(show_hours) {
        $('#dropdown-hours').css('display','none');
        show_hours = false;
      } else {
        $('#dropdown-hours').css('display','block');
        show_hours = true;
      }

    });

    $('#dropdown-item-dates-m').click(function(e){
      e.stopPropagation();

      $('#dropdown-hours-m').css('display','none');

      if(show_dates_m) {
        $('#dropdown-dates-m').css('display','none');
        show_dates_m = false;
      } else {
        $('#dropdown-dates-m').css('display','block');
        show_dates_m = true;
      }

    });

    $('#dropdown-item-hours-m').click(function(e){
      e.stopPropagation();

      $('#dropdown-dates-m').css('display','none');

      if(show_hours_m) {
        $('#dropdown-hours-m').css('display','none');
        show_hours_m = false;
      } else {
        $('#dropdown-hours-m').css('display','block');
        show_hours_m = true;
      }

    });

    $(document).click(function(){

      $('#dropdown-dates').css('display','none');
      show_dates = false;

      $('#dropdown-hours').css('display','none');
      show_hours = false;

      $('#dropdown-dates-m').css('display','none');
      show_dates_m = false;

      $('#dropdown-hours-m').css('display','none');
      show_hours_m = false;

    });


});

function updateUI(filterArray) {

  $('#tickets-area').empty();

  var myjson;
  $.getJSON('json/events.json', function(json) {
    myjson = json;

    var html_dropdown_dates = '';
    var html_dropdown_dates_m = '';
    var html_dropdown_hours = '';
    var hours = new Array();
    var fillHours = [];
    var days = new Array();
    var fillDays = [];
    var eventsArray = new Array();
    var eventsFilterArray = new Array();
    var ticket = {
      date: '',
      day: '',
      month: '',
      type: '',
      init: '',
      uri: ''
    };

    var html = "";
    $.each(json.events,function(i,item) {
      item.date = item.date.split('-').join('/');

      var date = new Date(item.date);
      var month = ["Ene", "Feb", "Mar", "Abr", "May", "Jun","Jul", "Ago", "Sep", "Oct", "Nov", "Dic"][date.getMonth()];
      var year = date.getFullYear();
      var day = date.getDate();

      if (date.valueOf() == filterArray[0].date.valueOf() ) {
          var clase;
          if(item.available==0){
              clase = 'boton_agotado';
              textobtn = "BOLETOS AGOTADOS";
          }

          if(item.available==1){
            clase = 'boton_boletos';
            textobtn = "COMPRA TUS BOLETOS";

            html += '<div class="row" style="padding-top:20px">'
                     + '<div class="col-md-offset-1 col-md-1 col-sm-1 col-xs-3" align="center">'
                       +  '<div class="caja_boletos">'
                         +  '<div class="txt_boletos_tl">' + day + '</div>'
                         +  '<div class="txt_boletos_til">' + month + '</div>'
                      +   '</div>'
                     +  '</div>'
                    + '<div class="col-md-5 col-sm-7 col-xs-12">'
                    +    '<div class="caja_boletos2">'
                    +      '<div class="txt_boletos_til2"><img src="'+(item.type == 'General' ? 'images/hv_skull_n.png' : 'images/hv_fire_n.png')+'" alt="">&nbsp;' + (item.type == 'General' ? 'Hotel de Leyendas Victoria ' : 'Hotel de Leyendas Victoria - Acceso Total ') + (item.init != '' ? (item.init + ' hrs.') : '') +  '</div>'
                    +     '<div class="txt_boletos">Paseo de la Reforma 109, Col. Tabacalera, Ciudad de México, DF 06030 Ciudad de México, CDMX Recuerda estar 20 minutos antes del evento</div>'
                    +    '</div>'
                    +  '</div>'
                    + '<div class="col-md-4 col-sm-4 col-xs-12 compra-boletos">'
                    +  '<button class="'+clase+'" onclick="launchUrl(\'' + item.url + '\',\'' + 'Botón - Compra tus boletos' + '\',\'' + day + ' ' + month  + ', Hotel de Leyendas Victoria - Acceso Total' + item.init + ' hrs.' + '\');" id="boton-ticket-url">'+textobtn+'</button>'
                    + '</div>'
                    + '</div>'
                    + '<div class="row" style="margin-top:30px">'
                    +  '<div class="col-md-offset-1 col-md-10 separador">&nbsp;</div>'
                    + '</div>';
          }

                hours.push(item.init);
                days.push(item.date);

                ticket = new Object();

                ticket.date = item.date;
                ticket.day = day;
                ticket.month = month;
                ticket.type = item.type;
                ticket.init = item.init;
                ticket.url = item.url;

                eventsArray.push(ticket);

      }


    });

    $('#tickets-area').append(html);

    $.each(days, function(i, day){
        if($.inArray(day, fillDays) === -1)
          fillDays.push(day);
    });

    fillDays.sort();

  });

}

function sendGoogleTagEvent(cat, label) {
  ga('send', {
        hitType: 'event',
        eventCategory: 'Boletos - ' + cat,
        eventAction: 'click',
        eventLabel: label
      });
}


function launchUrl(uri,cat,label) {
  sendGoogleTagEvent(String(cat),String(label));
  window.open(String(uri),'_blank');
}

<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class LeyendasController extends Controller {

  public function getUI(Request $request)
  {
    return $request->session()->get('allowed') ? view('leyendas') : redirect('home');
  }
}
?>

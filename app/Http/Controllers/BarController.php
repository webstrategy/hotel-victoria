<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class BarController extends Controller {

  public function getUI(Request $request)
  {
    return $request->session()->get('allowed') ? view('bar') : redirect('home');

  }
}
?>

<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\User as User;

date_default_timezone_set('America/Mexico_City');


class AdminController extends Controller {

  public function __construct()
  {
      $this->middleware('auth')->except('logout');
  }

  /**
   * Display a view.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
      return view('admin.admin');
  }

  /**
  * Handle an authentication attempt.
  *
  * @return Response
  */

  public function getSession(Request $request)
  {
    // $request
    $callback = array('status' => 200 ,'response' => $request->session()->get('admin_loged'));
    echo json_encode($callback);
  }

  public function setSession($user)
  {
    session(['admin_loged' => TRUE, 'user' => $user]);
    $callback = array('status' => 200,'response' => 'Session Admin Login Success');
    echo json_encode($callback);
  }

}
?>

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FeriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('logout');
    }

    /**
     * Display a view.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.feria');
    }

    /**
    * Handle an authentication attempt.
    *
    * @return Response
    */

   

}

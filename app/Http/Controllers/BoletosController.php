<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class BoletosController extends Controller {

  public function getUI(Request $request)
  {
    return $request->session()->get('allowed') ? view('boletos') : redirect('home');
  }
}
?>

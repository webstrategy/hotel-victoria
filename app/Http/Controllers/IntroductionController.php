<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class IntroductionController extends Controller {

  public function getUI(Request $request)
  {
    return $request->session()->get('allowed') ? view('intro.intro') : redirect('home');
  }
}
?>

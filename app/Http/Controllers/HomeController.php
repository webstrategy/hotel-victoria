<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
date_default_timezone_set('America/Mexico_City');
use DB;

class HomeController extends Controller {

  public function __construct()
  {

  }

  public function getUI(Request $request)
  {
    return view('home');
  }

  public function getSession(Request $request)
  {
    $callback = array('success' => TRUE ,'response' => $request->session()->get('allowed'));
    echo json_encode($callback);
  }

  public function setSession()
  {
    session(['allowed' => $_POST['allow']]);
    $callback = array('success' => TRUE, 'response' => 'Session data stored');
    echo json_encode($callback);
  }

  public function setUserData()
  {
    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $insert = DB::table('fb_users')->insert([
         'fullname' => $_POST['name'],
         'email' => $_POST['email'],
         'gender' => $_POST['gender'],
         'age_range' => $_POST['age_range'],
         'facebookUserId' => $_POST['id'],
         'createdAt' => date("Y-m-d H:i:s"),
         'updatedAt' => date("Y-m-d H:i:s"),
         'browser' => $this->getBrowser(),
         'os' => $this->getOS(),
         'ip' => $_SERVER['REMOTE_ADDR'],
         'device' => '',
         'user_info' => $user_agent
       ]);

    if($insert) {
      $callback = array('success' => TRUE,'response' => 'Session data stored');
    }else{
      $callback = array('success' => FALSE,'response' => 'Couldn´t store user data');
    }

    DB::disconnect('sqlsrv');

    // Insert Facebook Data on marketingcloud
    $data = array(
      'id' => $_POST['id'],
      'first_name' => $_POST['first_name'],
      'last_name' => $_POST['last_name'],
      'email' => $_POST['email'],
      'age' => $_POST['age_range'],
      'gender' => $_POST['gender'],
      'source' => 'hotel_victoria'
    );

    $this->MarketingCloud($data);

    echo json_encode($callback);
  }

  public function MarketingCloud($data) {

    $exactTarget = 'https://auth.exacttargetapis.com/v1/requestToken';
    $OAUTHarray = array('clientId' => 'di0n2rj36y2n7ephjjzwvijw', 'clientSecret' => 'Qes89tNOgOe2CD47X6amKjqg');

    $options = array(
            'http' => array(
                    'method'  => 'POST',
                    'content' => json_encode( $OAUTHarray ),
                    'header'=>  "Content-Type: application/json\r\n" .
                    "Accept: application/json\r\n"
            )
    );

    $context  = stream_context_create( $options );
    $result = file_get_contents( $exactTarget, false, $context );
    $response = json_decode( $result );

    $token = $response->accessToken;


    $key_secret = "A7AD0FE0-3BAB-469C-A61B-7B2FDC1AC01B";

    $exactTarget2 = 'https://www.exacttargetapis.com/hub/v1/dataevents/key:'.$key_secret.'/rowset';

    $OAUTHarray2 = array(
      array(
      'keys'=> array('FacebookID'=> $data['id']),
      'values'=>array('FirstName' => $data['first_name'], 'LastName' => $data['last_name'], 'EmailAddress'=> $data['email'], 'Age' => $data['age'], 'Gender' => $data['gender'],  'Source' => $data['source'])
      )
    );

    $options2 = array(
            'http' => array(
                    'method'  => 'POST',
                    'content' => json_encode( $OAUTHarray2 ),
                    'header'=>  "Content-Type: application/json\r\n" .
                    "Accept: application/json\r\n".
                    "Authorization: Bearer ".$token."\r\n"
            )
    );

    $context2  = stream_context_create( $options2 );
    $result2 = file_get_contents( $exactTarget2, false, $context2 );

  }

public function getOS() {

  $user_agent = $_SERVER['HTTP_USER_AGENT'];

  $os_platform = "Unknown OS Platform";

  $os_array = array(
                    '/windows nt 10/i' => 'Windows 10',
                    '/windows nt 6.3/i' => 'Windows 8.1',
                    '/windows nt 6.2/i' => 'Windows 8',
                    '/windows nt 6.1/i' => 'Windows 7',
                    '/windows nt 6.0/i' => 'Windows Vista',
                    '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
                    '/windows nt 5.1/i' => 'Windows XP',
                    '/windows xp/i' => 'Windows XP',
                    '/windows nt 5.0/i' => 'Windows 2000',
                    '/windows me/i' => 'Windows ME',
                    '/win98/i' => 'Windows 98',
                    '/win95/i' => 'Windows 95',
                    '/win16/i' => 'Windows 3.11',
                    '/macintosh|mac os x/i' => 'Mac OS X',
                    '/mac_powerpc/i' => 'Mac OS 9',
                    '/linux/i' => 'Linux',
                    '/ubuntu/i' => 'Ubuntu',
                    '/iphone/i' => 'iPhone',
                    '/ipod/i' => 'iPod',
                    '/ipad/i' => 'iPad',
                    '/android/i' => 'Android',
                    '/blackberry/i' => 'BlackBerry',
                    '/webos/i' => 'Mobile'
                    );

  foreach ($os_array as $regex => $value) {

    if (preg_match($regex, $user_agent)) {
        $os_platform    =   $value;
    }
  }

  return $os_platform;

}

public function getBrowser() {

  $user_agent = $_SERVER['HTTP_USER_AGENT'];

  $browser = "Unknown Browser";

  $browser_array = array(
                    '/mobile/i' => 'Handheld Browser',
                    '/msie/i' => 'Internet Explorer',
                    '/firefox/i' => 'Firefox',
                    '/safari/i' => 'Safari',
                    '/chrome/i' => 'Chrome',
                    '/edge/i' => 'Edge',
                    '/opera/i' => 'Opera',
                    '/netscape/i' => 'Netscape',
                    '/maxthon/i' => 'Maxthon',
                    '/konqueror/i' => 'Konqueror'
                      );

  foreach ($browser_array as $regex => $value) {

    if (preg_match($regex, $user_agent)) {
        $browser    =   $value;
    }
  }

  return $browser;

}


}
?>

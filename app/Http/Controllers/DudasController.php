<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class DudasController extends Controller {

  public function getUI(Request $request)
  {
    return $request->session()->get('allowed') ? view('dudas') : redirect('home');

  }
}
?>

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\User as User;

class AdminLoginController extends Controller
{
    /**
    * Handle an authentication attempt.
    *
    * @return Response
    */

    /**
     * Display a view.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        return view('admin.login');
    }

    public function authenticate(Request $request)
    {
      // Make Rules validations
      $rules = [
        'password' => 'required',
        'email' => 'required|email',
      ];

      // Make custom messages of rules
      $messages = [
        'required' => 'El campo es requerido.',
        'alpha' => 'El campo solo debe de contener letras.',
        'alpha_spaces' => 'El campo solo debe de contener letras y espacios.',
        'alpha_num' => 'El campo solo debe de contener letras y números.',
        'digits' => 'El campo debe de ser :digits dígitos.',
        'numeric' => 'El campo solo debe de contener números.',
        'email' => 'El campo :attribute debe de ser una dirección de correo válida.',
      ];

      /* ------  Make Form validation ------- */
      $this->validate($request, $rules, $messages);


      $user_exist = User::where('email', $request->email)
                          ->where('type', 1)
                          ->get();

      $user_exist_count = count($user_exist);

      if ($user_exist_count > 0) {

          if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
              // Authentication passed...
              return redirect()->intended('admin');
          } else {
              return redirect()->back()
                      ->withErrors([
                          'password' => 'Los Datos no son correctos, intente de nuevo.',
                          'email' => 'Los Datos no son correctos, intente de nuevo.',
                        ])
                      ->withInput();
          }

      } else {
        return redirect()->back()
                        ->withErrors([
                            'password' => 'Los Datos no son correctos, intente de nuevo.',
                            'email' => 'Los Datos no son correctos, intente de nuevo.',
                          ])
                        ->withInput();
      }
    }

    public function logout()
    {
      Auth::logout();
      return redirect()->intended('login');
    }

    public function get_user(Request $request)
    {
      $user = Auth::user();

      return $user;
    }

}

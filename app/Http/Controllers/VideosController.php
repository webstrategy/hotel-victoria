<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class VideosController extends Controller {

  public function getUI(Request $request)
  {
    return $request->session()->get('allowed') ? view('videos') : redirect('home');
  }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Victoria</title>
    <link rel="stylesheet" href="intro/styles/style.css">
    <script>
        var VICTORIA = {}
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<meta name="viewport" content="width=device-width, minimal-ui">-->
</head>

<body class="block-scroll">
    <div class="site">
        <div class="relative">
            <div class="global-skip-intro">
                <a href="home">Saltar Intro</a>
            </div>
            <!--<header>-->
            <!--&lt;!&ndash;<div class="container">&ndash;&gt;-->
            <!--</header>-->
            <!--<div class="fake-header"></div>-->
            <div class="front-ui">
                <div class="cancel-infinite hidden">
                    Cancelar Scroll Infinito
                </div>

                <div class="logo">
                    <div class="relative" id="logo-parallax">
                        <img src="intro/images/logo_hotel-victoria.png" alt="Hotel Victoria" class="layer" data-depth="0.1">
                        <img src="intro/images/logo_shadow.png" alt="Hotel Victoria" class="layer logo-shadow" data-depth="0.9">
                    </div>
                    <h1>Hotel Victoria</h1>
                </div>

                <div class="mute-songs">
                    <img src="intro/images/icons/volumen_on.png" alt="Volumen activo" class="volume_on">
                    <img src="intro/images/icons/volumen_off.png" alt="Volumen activo" class="volume_off">
                </div>

                <div class="elevator-cta elevator-exit">
                    <div class="relative">
                        <div class="circle-style circle-static"></div>
                        <div class="circle-style circle-1"></div>
                        <div class="circle-style circle-2"></div>
                        <div class="circle-style circle-3"></div>
                        <div class="circle-style circle-4"></div>
                    </div>
                </div>
                <div class="elevator-cta elevator-enter">
                    <div class="relative">
                        <div class="circle-style circle-static"></div>
                        <div class="circle-style circle-1"></div>
                        <div class="circle-style circle-2"></div>
                        <div class="circle-style circle-3"></div>
                        <div class="circle-style circle-4"></div>
                    </div>
                </div>

                <div class="volume-recomendation">
                    <div class="text">
                        <div style="height: 88px;">
                            <img src="intro/images/instructions.png" alt="Instrucciones de volumen">
                        </div>
                        <p>Conecta tus audífonos</p>
                        <p class="second">para mejorar tu experiencia</p>
                        <div class="loading">
                            <span class="span-inline">Cargando</span>
                            <div class="loader-bar">
                                <div class="padding">
                                    <div class="line" style="width: 4px;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="ready hidden">
                            <button class="btn startTravel">Entrar</button>
                        </div>
                    </div>
                    <div class="relative">
                        <div class="circle-style circle-static"></div>
                        <div class="circle-style circle-1"></div>
                        <div class="circle-style circle-2"></div>
                        <div class="circle-style circle-3"></div>
                        <div class="circle-style circle-4"></div>
                    </div>
                </div>

                <div class="scroll-vertical">
                    <div class="black">
                        <div class="wrapper">
                            <div class="text">Scroll para continuar</div>
                        </div>
                    </div>
                </div>
                <div class="scroll-horizontal">
                    <div class="black">
                        <div class="wrapper">
                            <div class="text">Explora el pasillo</div>
                        </div>
                    </div>
                </div>

                <div class="hand-clicker" id="single-hand"></div>
                <div class="massive-click">
                    <div class="vertical-line"></div>
                    <div class="padding">
                        <div class="black-circle">
                            <div class="text">
                                <div class="text-1">Presiona<br/>para escapar</div>
                                <div class="text-2">¡Más<br/>rapido!</div>
                            </div>
                        </div>
                        <div class="white-circle"></div>
                    </div>
                    <div class="arc">
                        <div id="cont" data-pct="0">
                            <svg id="svg" width="130" height="130" viewport="0 0 0 0 " version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <circle r="50" cx="80" cy="65" fill="transparent" stroke-dasharray="330.288" stroke-dashoffset="0"></circle>
                                <circle id="bar" r="50" cx="65" cy="65" fill="transparent" stroke-dasharray="330.288" stroke-dashoffset="0" style="stroke-dashoffset: 330.288px;"></circle>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <div class="site-content">
                <div class="content">
                    <div class="home-block">
                        <div class="vertical-aligment">
                            <div class="text">
                                <!--<h1>HOTEL VICTORIA</h1>-->
                                <button class="startTravel hidden">Entrar</button>
                                <!--<div class="cta">-->
                                <!--</div>-->
                            </div>
                        </div>

                        <ul id="scene" data-friction-x="0.3" data-friction-y="0.5">
                            <li class="layer" id="specks" data-depth="0.1"></li>
                            <li class="layer" id="layer-1" data-depth="0.15">
                                <div class="img" id="img-1"></div>
                            </li>
                            <li class="layer" id="layer-2" data-depth="0.25">
                                <div class="img" id="img-2"></div>
                            </li>
                            <li class="layer" id="layer-3" data-depth="0.45">
                                <div class="img" id="img-3"></div>
                            </li>
                        </ul>

                        <div class="scenes">
                            <div class="vignette"></div>
                            <div class="scene scene-1">
                                <div class="backgrounds background-1"></div>
                                <div class="dama"></div>
                                <div class="dante"></div>
                                <div class="comic-text">
                                    <div class="rec">
                                        Alguien como tú no debía caminar a solas por aquí.
                                    </div>
                                </div>
                            </div>
                            <div class="scene scene-2">
                                <div class="backgrounds background-4"></div>
                                <div class="black"></div>
                                <div class="dante"></div>
                                <div class="door"></div>
                                <div class="door-2"></div>
                                <div class="arm"></div>
                                <div class="comic-text">
                                    <div class="rec">
                                        Sobre todo en temporada de caza.
                                    </div>
                                </div>
                            </div>
                            <div class="scene scene-3 up-black">
                                <div class="outside"></div>
                                <div class="backgrounds background-5"></div>
                                <div class="elevator-numbers">
                                    <ul>
                                        <li><span>1</span></li>
                                        <li><span>2</span></li>
                                        <li><span>3</span></li>
                                        <li><span>4</span></li>
                                        <li><span>5</span></li>
                                        <li><span>6</span></li>
                                        <li class="active"><span>7</span></li>
                                        <li><span>8</span></li>
                                        <li><span>9</span></li>
                                        <li><span>10</span></li>

                                    </ul>
                                </div>
                                <div class="dante"></div>
                                <div class="dama"></div>
                                <div class="comic-text" id="dialog3">
                                    <div class="rec">
                                        Amor, te sigo a donde quieras.
                                    </div>
                                </div>

                                <div class="comic-text" id="dialog4">
                                    <div class="rec">
                                        No iba a esperar un no como respuesta.
                                    </div>
                                </div>
                            </div>
                            <div class="scene scene-4">
                                <div class="flashing-black"></div>
                                <div class="pano" id="pano">
                                    <div class="wrapper">
                                        <div class="aux" id="samePanoWidth">
                                            <img src="intro/images/scenes/mob.png" alt="Pano" class="mob" id="dropThisMob">
                                            <img src="intro/images/scenes/pano.jpg" alt="Pano" class="bg">
                                        </div>
                                    </div>
                                </div>
                                <div class="comic-text" id="dialog5">
                                    <div class="rec">
                                        No iba a dejarte escapar.
                                    </div>
                                </div>
                            </div>
                            <div class="scene scene-5 up-black">
                                <div class="backgrounds background-5"></div>
                                <div class="elevator-numbers" id="elevator-last-interaciton">
                                    <ul>
                                        <li><span>1</span></li>
                                        <li><span>2</span></li>
                                        <li class="active"><span>3</span></li>
                                        <li><span>4</span></li>
                                        <li><span>5</span></li>
                                        <li><span>6</span></li>
                                        <li><span>7</span></li>
                                        <li><span>8</span></li>
                                        <li><span>9</span></li>
                                        <li><span>10</span></li>

                                    </ul>
                                </div>
                                <div class="arm-left"></div>
                                <div class="arm-right"></div>
                                <div class="black-shadow" id="black-shadow-last"></div>
                                <div class="comic-text" id="dialog6">
                                    <div class="rec">
                                        Después de todo, prometiste que me seguirias
                                    </div>
                                </div>

                                <div class="over-final"></div>
                                <div class="blood-splash"></div>
                            </div>
                            <div class="scene scene-6 up-black">
                                <div class="backgrounds background-8">
                                    <div class="elevator-numbers">
                                        <ul class="elevator-list">
                                            <li><span>1</span></li>
                                            <li><span>2</span></li>
                                            <li><span>3</span></li>
                                            <li><span>4</span></li>
                                            <li><span>5</span></li>
                                            <li><span>6</span></li>
                                            <li><span>7</span></li>
                                            <li><span>8</span></li>
                                            <li><span>9</span></li>
                                            <li><span>10</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="last-dama-1"></div>
                                <div class="last-dama-2"></div>
                                <div class="last-dama-3"></div>
                                <div class="cuenta-la-leyenda">
                                    <p>Cuenta la leyenda que una <strong class="red">dama de rojo</strong> seducia a sus victimas
                                        para después asesinarlas</p>
                                </div>
                                <div class="cal-to-action" id="last-message">
                                    <p>Es momento de vivir la experiencia de <strong id="flashing">Hotel Victoria</strong> en
                                        todo su <span class="tachado">esplendor</span> horror</p>
                                    <a href="boletos" class="btn" stylew="cursor: pointer;" target="_blank">Ver Boletos</a>
                                </div>
                                <div class="black-bg"></div>
                            </div>
                        </div>
                    </div>
                    <div class="floors-divs">
                        <section></section>
                        <section></section>
                        <section></section>
                        <section></section>
                        <section></section>
                        <section></section>
                        <section></section>
                        <!--<div class="infinite-loop">-->
                        <!--<section class="duplicate-content"></section>-->
                        <!--<section class="no-padding">-->
                        <!--<div class="pano">-->
                        <!--<div class="wrapper">-->
                        <!--<img src="intro/images/pano.jpg" alt="Pano">-->
                        <!--</div>-->
                        <!--</div>-->
                        <!--</section>-->
                        <!--<section>-->
                        <!--</section>-->
                        <!--</div>-->
                    </div>

                </div>
            </div>
        </div>
    </div>
</body>
<script src="intro/scripts/libs/jquery-3.1.1.min.js"></script>
<script src="intro/scripts/libs/preloadjs-0.6.2.min.js"></script>
<script src='intro/scripts/libs/parallax.js'></script>
<script src='intro/scripts/libs/pizzicato.js'></script>
<script src="intro/scripts/home.js"></script>

<script>
    $(document).ready(function () {
        VICTORIA.home.init();
    });

</script>

</html>

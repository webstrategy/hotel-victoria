@extends('base')

@section('metakey', ', hotel victoria.')

@section('title', 'Hotel Victoria - dudas')
@section('active-home', 'active')

@section('header')
	@include('partials._header')
@endsection

@section('content')

<div class="container-fluid ">
   <div class="row" style="padding-top:50px">
      <div class="col-md-offset-2 col-md-8 col-sm-12 col-xs-12">
        <div class="titulos"><u style="text-decoration: none; border-bottom: 5px solid #ea002a; padding-bottom:7px;">DUD</u>AS HOTEL VICTORIA</div>
      </div>
    </div>

    <div class="row" style="padding-top:30px">
      <div class="col-md-offset-2 col-md-8 col-sm-12 col-xs-12">
				<div class="" style="padding-top:30px">
        	<div class="txt_boletos_til2">1. ¿A partir de cuándo y hasta qué fecha se podrá visitar el Hotel de Leyendas Victoria?</div>
          <div class="txt_boletos"><i><strong>#HotelVictoria</strong></i> abrirá sus puertas el 25 de octubre y estará abierto hasta el 30 de noviembre de 2017, de martes a sábado, en un horario de 19:00 a 23:00 hrs., mientras que los domingos será de 18:00 a 22:00 hrs.
          </div>
				</div>

				<div class="" style="padding-top:30px">
        	<div class="txt_boletos_til2">2. ¿En dónde se encuentra ubicado el Hotel de Leyendas Victoria?</div>
          <div class="txt_boletos">En Paseo de la Reforma No. 109, Col. Tabacalera en la delegación Cuauhtémoc.</div>
				</div>

				<div class="" style="padding-top:30px">
        	<div class="txt_boletos_til2">3. ¿Dónde se podrán adquirir los boletos para acceder al Hotel Victoria?</div>
          <div class="txt_boletos">Los boletos se encontrarán a la venta en <a style="color:#666" href="http://www.ticketmaster.com.mx/"   target="_blank"><strong>Ticketmaster</strong></a> con un costo de $420.00 MXN más cargo, a partir del 9 de octubre del 2017.
          </div>
				</div>

				<div class="" style="padding-top:30px">
        	<div class="txt_boletos_til2">4. ¿El inmueble es seguro?</div>
          <div class="txt_boletos">Se llevó a cabo una exhaustiva revisión a las instalaciones del Hotel por parte de instancias
						oficiales resultando que se encuentra en perfectas condiciones y sin daños estructurales.</div>
				</div>

				<div class="" style="padding-top:30px">
					<div class="txt_boletos_til2">5. ¿Cuál es el dictamen de protección civil en cuanto al inmueble después del sismo?</div>
        	<div class="txt_boletos">Tanto protección civil como diversos arquitectos valoraron el inmueble y se encuentra en perfecto estado, por lo que el Hotel de Leyendas Victoria, tanto en el área de la puesta en escena como en el bar, el sótano y los 11 pisos que lo componen pueden operar sin problema alguno.</div>
				</div>

				<div class="" style="padding-top:30px">
					<div class="txt_boletos_til2">6. ¿Por qué se atrasó la fecha de apertura del Hotel de Leyendas Victoria?</div>
        	<div class="txt_boletos">En Grupo Modelo y en Victoria estamos comprometidos con México y la sociedad por lo que debido a los acontecimientos recientes se decidió posponer la apertura del Hotel Victoria como símbolo de respeto y unión a todos los mexicanos y a nuestro país en un momento tan difícil.</div>
				</div>

				<div class="" style="padding-top:30px">
					<div class="txt_boletos_til2">7. ¿Es segura la zona en la que se encuentra el Hotel de Leyendas Victoria?</div>
        	<div class="txt_boletos">Para determinar que el Hotel de Leyendas Victoria se encuentra en perfectas condiciones, también se evaluaron las zonas aledañas al inmueble, los accesos y las vías de comunicación, las cuales se encuentran en perfectas condiciones, por lo que nuestros visitantes se pueden sentir seguros al visitar esta experiencia.</div>
				</div>

				<div class="" style="padding-top:30px">
					<div class="txt_boletos_til2">8. ¿Qué medidas han tomado para situaciones de emergencia dentro del Hotel de Leyendas Victoria?</div>
        	<div class="txt_boletos">Nuestro personal de seguridad y de operación dentro del Hotel de Leyendas Victoria se encuentra calificado para atender cualquier emergencia, además de que se cuenta con la certificación de Protección Civil, así como los señalamientos de seguridad perfectamente identificados. Contaremos con todos los servicios médicos y de emergencia necesarios para atender cualquier situación.</div>
				</div>

				<div class="" style="padding-top:30px">
					<div class="txt_boletos_til2">9. Si compré boletos en la preventa para las fechas que no se llevarán a cabo ¿Qué puedo hacer?</div>
					<div class="txt_boletos"> Para todos aquellos que cuentan con boletos de una fecha anterior, se les hará un reembolso, a través de Ticketmaster, ya sea por teléfono, internet o en los centros de venta, con el fin de que puedan elegir una nueva fecha para vivir la experiencia inmersiva que ha causado sensación.</div>
				</div>

				<div class="" style="padding-top:30px">
					<div class="txt_boletos_til2">10. ¿La experiencia del Hotel de Leyendas Victoria está abierta para el público en general?</div>
					<div class="txt_boletos">La experiencia que ofrecemos es para mayores de edad (+18 años), y para aquellas personas que no sean sensibles a estos temas y que no padezcan alguna afección cardíaca.</div>
				</div>
	    	</div>
			</div>
		<!-- <div class="row">
      <div class="col-md-offset-1 col-md-10 separador">&nbsp;</div>
    </div> -->






</div>
@endsection

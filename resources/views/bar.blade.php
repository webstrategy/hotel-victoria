@extends('base')

@section('metakey', ', hotel victoria.')

@section('title', 'Hotel Victoria - En el bar')
@section('active-home', 'active')

@section('header')
	@include('partials._header')
@endsection

@section('content')
<div class="container-fluid">

  <div class="row" style="margin-left: -15px; margin-right: -15px;">
    <div class="bar">
      <div class="col-md-offset-2 col-md-8" align="center">
        <div class="txt_opacity" style="margin-top:90px">Ahora en  Hotel Victoria</div>
        <div class="titulos_ley hidden-xs">EL <u style="text-decoration: none; border-bottom: 5px solid #ea002a; padding-bottom:1px;">B</u>AR</div>
        <!-- <div class="titulos_bar hidden-xs"><u style="text-decoration: none; border-bottom: 5px solid #fff; padding-bottom:1px;">DJ</u>'S</div>
        <div class="txt_ley hidden-xs">The arresting billboards—which feature a sepia and white houndstooth motif and a male model clad in the classic pattern and mirrored shades—appeared overnight in the Capitol  and quickly caused rubbernecking.
        </div> -->
<!-- MOBILE -->
        <div class="titulos_ley_m visible-xs">EL <u style="text-decoration: none; border-bottom: 5px solid #ea002a; padding-bottom:1px;">B</u>AR</div>
        <div class="titulos_bar_m visible-xs"><u style="text-decoration: none; border-bottom: 5px solid #fff; padding-bottom:1px;">DJ</u>'S</div>
        <div class="txt_ley_m visible-xs">The arresting billboards—which feature a sepia and white houndstooth motif and a male model clad in the classic pattern and mirrored shades—appeared overnight in the Capitol  and quickly caused rubbernecking.
        </div>
        <div class="txt_opacity" style="margin-top:70px">*EVENTO PARA MAYORES DE EDAD</div>



      </div>
    </div>
    <div class="boton_bar col-md-offset-1 col-md-10 hidden-sm hidden-xs" style="" align="center" >
      <img id="myImage"  onclick="changeImage()"  src="images/hv_eb_drink.png" width="160" height="64">
      <img id="myImage1" onclick="changeImage1()"  src="images/hv_eb_food.png" width="160" height="64">
      <img id="myImage2" onclick="changeImage2()"  src="images/hv_eb_music.png" width="160" height="64">
    </div>
    <div class="boton_bar_m col-sm-12 col-xs-12 visible-sm visible-xs" style="" align="center">
      <img id="myImage4" onclick="changeImage4()"  src="images/hv_eb_drink.png" width="90" height="38">
      <img id="myImage5" onclick="changeImage5()" src="images/hv_eb_food.png"  width="90" height="38">
      <img id="myImage6" onclick="changeImage6()" src="images/hv_eb_music.png" width="90" height="38">
    </div>
  </div>

  <div class="row">
    <div class="col-md-offset-2 col-md-8" align="center" style="margin-top:10px">
          <img class="img-responsive" src="images/hv_elbar_galeria.jpg" alt="">
    </div>
  </div>



        <div class="row" style="margin-top:10px">
          <div class="col-md-12 testimonios hidden-xs">
            <div class="testimonio">
                <div class="testi"> Testimonios
              <div class="txt_testimonio">
                  “Es una gran experiencia, sabes que sucederá algo pero no sabes qué, vas de sorpresa en sorpresa.”
                </div>
                <strong>Hijo del Santo</strong>
               </div>
            </div>
          </div>
<!-- MOBILE -->
          <div class="col-xs-12 col-sm-12 testimonios visible-xs" style="margin-top:10px">
            <div class="testimonio_m">
                <div class="testi_m"> Testimonios
                <div class="txt_testimonio_m">
                  “Es una gran experiencia, sabes que sucederá algo pero no sabes qué, vas de sorpresa en sorpresa.”
                </div>
              <strong>Hijo del Santo</strong>
               </div>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12" align="center" >
            <div class="txt_int">¿Quieres pasarla increíble en el Bar Victoria?<br>Solicita tu entrada aquí</div>
            <!-- <input class="boton_boletos"type="button" value="SOLICITAR ENTRADAS" id="boton"> -->
          </div>
        </div>


</div>

@endsection

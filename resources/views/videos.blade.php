@extends('base')

@section('metakey', ', videos, hotel victoria.')

@section('title', 'Hotel Victoria - Videos')
@section('active-home', 'active')

@section('header')
	@include('partials._header')
@endsection

@section('content')
<div class="container-fluid ">

  <div class="row" style="margin-left: -15px; margin-right: -15px;">
    <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12"  style="padding-top:80px">
        <div class="titulos"><u style="text-decoration: none; border-bottom: 5px solid #ea002a; padding-bottom:7px;">VI</u>DEOS</div>
    </div>
  </div>

  <div class="row">
		<div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12 txt_boletos_til2">
			Hotel de Leyendas Victoria una coproducción de Cultura Colectiva y Argos.<br>Thriller de 10 capítulos que promete aterrorizarte y mantenerte al filo del suspenso. ¡No te los pierdas! Todos los días a las 10 pm.
		</div>
    <!-- <div class="col-md-offset-6 col-md-4 hidden-sm hidden-xs" align="left">
      <button class="boton_canales" ><img src="images/hv_canal1.png" alt=""/></button>
      <button class="boton_canales" ><img src="images/hv_canal2.png" alt=""/></button>
      <button class="boton_canales" ><img src="images/hv_canal3.png" alt=""/></button>
    </div> -->
<!-- MOBILE -->
    <!-- <div class="col-sm-12 col-xs-12 visible-sm visible-xs" align="center">
      <button class="boton_canales_m" ><img src="images/hv_canal1.png" alt=""/></button>
      <button class="boton_canales_m" ><img src="images/hv_canal2.png" alt=""/></button>
      <button class="boton_canales_m" ><img src="images/hv_canal3.png" alt=""/></button>
    </div> -->
  </div>

    <div class="row" style="padding-top:50px">
      <div class="col-md-offset-1 col-md-10 hidden-sm hidden-xs" align="left">
				<iframe width="450" height="253" style="margin:15px" src="https://www.youtube.com/embed/P4HW4ZZPi3A" frameborder="0" allowfullscreen></iframe>
				<iframe width="450" height="253" style="margin:15px" src="https://www.youtube.com/embed/Fn3gxyMJ9RY" frameborder="0" allowfullscreen></iframe>
				<iframe width="450" height="253" style="margin:15px" src="https://www.youtube.com/embed/oMx1KbbIIcU" frameborder="0" allowfullscreen></iframe>
				<iframe width="450" height="253" style="margin:15px" src="https://www.youtube.com/embed/yE6KtJQKJcM" frameborder="0" allowfullscreen></iframe>
				<iframe width="450" height="253" style="margin:15px" src="https://www.youtube.com/embed/7rGE_u_gQN8" frameborder="0" allowfullscreen></iframe>
				<iframe width="450" height="253" style="margin:15px" src="https://www.youtube.com/embed/-NECYjg-mD4" frameborder="0" allowfullscreen></iframe>
				<iframe width="450" height="253" style="margin:15px" src="https://www.youtube.com/embed/JEOuf-hDAuU" frameborder="0" allowfullscreen></iframe>
				<iframe width="450" height="253" style="margin:15px" src="https://www.youtube.com/embed/w3yG6L-_Cyk" frameborder="0" allowfullscreen></iframe>
				<iframe width="450" height="253" style="margin:15px" src="https://www.youtube.com/embed/dJIUO4A1XGY" frameborder="0" allowfullscreen></iframe>
				<iframe width="450" height="253" style="margin:15px" src="https://www.youtube.com/embed/VJd3a4MN_X4" frameborder="0" allowfullscreen></iframe>

			  <!-- <iframe style="margin:10px" width="700" height="394" src="https://www.youtube.com/embed/X_bOwcDwlKo" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="col-md-offset-1 col-md-2 hidden-sm hidden-xs">
        <iframe style="margin:10px" width="224" height="126" src="https://www.youtube.com/embed/X_bOwcDwlKo" frameborder="0" allowfullscreen></iframe>
        <iframe style="margin:10px" width="224" height="126" src="https://www.youtube.com/embed/X_bOwcDwlKo" frameborder="0" allowfullscreen></iframe>
        <iframe style="margin:10px" width="224" height="126" src="https://www.youtube.com/embed/X_bOwcDwlKo" frameborder="0" allowfullscreen></iframe> -->
      </div>
<!-- MOBILE -->


      <div class="col-xs-12 col-sm-12 visible-sm visible-xs" align="center">
				<iframe width="300" height="197" src="https://www.youtube.com/embed/P4HW4ZZPi3A" frameborder="0" allowfullscreen></iframe>
				<iframe width="300" height="197" src="https://www.youtube.com/embed/Fn3gxyMJ9RY" frameborder="0" allowfullscreen></iframe>
				<iframe width="300" height="197" src="https://www.youtube.com/embed/oMx1KbbIIcU" frameborder="0" allowfullscreen></iframe>
				<iframe width="300" height="197" src="https://www.youtube.com/embed/yE6KtJQKJcM" frameborder="0" allowfullscreen></iframe>
				<iframe width="300" height="197" src="https://www.youtube.com/embed/7rGE_u_gQN8" frameborder="0" allowfullscreen></iframe>
				<iframe width="300" height="197" src="https://www.youtube.com/embed/-NECYjg-mD4" frameborder="0" allowfullscreen></iframe>
				<iframe width="300" height="197" src="https://www.youtube.com/embed/JEOuf-hDAuU" frameborder="0" allowfullscreen></iframe>
				<iframe width="300" height="197" src="https://www.youtube.com/embed/w3yG6L-_Cyk" frameborder="0" allowfullscreen></iframe>
				<iframe width="300" height="197" src="https://www.youtube.com/embed/dJIUO4A1XGY" frameborder="0" allowfullscreen></iframe>
				<iframe width="300" height="197" src="https://www.youtube.com/embed/VJd3a4MN_X4" frameborder="0" allowfullscreen></iframe>

        <!-- <iframe style="margin:5px" width="300" height="197" src="https://www.youtube.com/embed/X_bOwcDwlKo" frameborder="0" allowfullscreen></iframe> -->
      </div>
      <!-- <div class="col-sm-12 col-xs-12 visible-sm visible-xs" align="center">
        <iframe style="margin:5px" width="300" height="197" src="https://www.youtube.com/embed/X_bOwcDwlKo" frameborder="0" allowfullscreen></iframe>
        <iframe style="margin:5px" width="300" height="197" src="https://www.youtube.com/embed/X_bOwcDwlKo" frameborder="0" allowfullscreen></iframe>
        <iframe style="margin:5px" width="300" height="197" src="https://www.youtube.com/embed/X_bOwcDwlKo" frameborder="0" allowfullscreen></iframe>
      </div> -->
    </div>

    <!-- <div class="row">
      <div class="col-md-offset-2 col-md-4 hidden-sm hidden-xs" align="left">
        <button class="boton_vid" ><img src="images/hv_seguir.png" alt=""/>&nbsp;&nbsp;SEGUIR</button>
        <button class="boton_vid" ><img src="images/hv_gusta.png" alt=""/>&nbsp;&nbsp;ME GUSTA</button>
        <button class="boton_vid" ><img src="images/hv_comp.png" alt=""/>&nbsp;&nbsp;COMPARTIR</button>
      </div> -->
<!-- MOBILE -->
      <!-- <div class="col-sm-12 col-xs-12 visible-sm visible-xs" align="center">
        <button class="boton_vid_m" ><img src="images/hv_seguir.png" alt=""/><br></button>
        <button class="boton_vid_m" ><img src="images/hv_gusta.png" alt=""/><br></button>
        <button class="boton_vid_m" ><img src="images/hv_comp.png" alt=""/><br></button>
      </div>
    </div> -->

</div>
@endsection

@extends('admin.base')

@section('title', 'Victoria - Ferias')

@section('header')
	@include('admin._header')
@endsection

@section('content')
<div class="container-fluid admin-content">
  <div class="row">

		@include('admin._sidebar')

    <div class="col admin-main-container">
      <div class="col">
        <div class="card">
          <div class="card-header">
            Ferias
          </div>
          <div class="card-body">

            <div class="row">
              <div class="col-lg-3 col-md-3">

                <form class="form form-horizontal" role="form" method="POST" action="#">
                  {{ csrf_field() }}
                  <div class="form-group row">
                    <div class="col">
                      <label for="name">Nombre</label>
                      <div class="input-group mb-2">
                        <input type="text" class="form-control login-form-btn" id="name" name="name">
                      </div>
                      <small class="text-warning">{{ $errors->first('name') }}</small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col">
                      <label for="description">Descripcion</label>
                      <div class="input-group mb-2">
                        <input type="text" class="form-control login-form-btn" id="description" name="description">
                      </div>
                      <small class="text-warning">{{ $errors->first('description') }}</small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col">
                      <label for="logo">Logo</label>
                      <input type="file" class="" id="logo" name="logo">
                      <small class="text-warning">{{ $errors->first('logo') }}</small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col">
                      <label for="fecha_start">Fecha Inicio</label>
                      <input type="text" class="form-control login-form-btn" id="fecha_start" name="fecha_start">
                      <small class="text-warning">{{ $errors->first('fecha_start') }}</small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col">
                      <label for="fecha_end">Fecha Fin</label>
                      <input type="text" class="form-control login-form-btn" id="fecha_end" name="fecha_end">
                      <small class="text-warning">{{ $errors->first('fecha_end') }}</small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col">
                      <label for="lat">Ubicación Lat</label>
                      <input type="text" class="form-control login-form-btn" id="lat" name="lat">
                      <small class="text-warning">{{ $errors->first('lat') }}</small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col">
                      <label for="lng">Ubicación Lng</label>
                      <input type="text" class="form-control login-form-btn" id="lng" name="lng">
                      <small class="text-warning">{{ $errors->first('lng') }}</small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col text-center">
                      <button type="#" class="btn feria-btn">Guardar</button>
                    </div>
                  </div>
                </form>

              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <div id='app'>
      <example></example>
    </div>

  </div>
</div>

<script src="js/app.js"></script>

@endsection

<!DOCTYPE html>
<html lang="es-mx">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Encode+Sans+Expanded:300,400,500,600" rel="stylesheet">
        <!-- BOOTSTRAP 4 -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <!-- STYLES -->
        <link rel="stylesheet" href="{{ asset('css/ferias/admin.css') }}">
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        <!-- Custom Style-->
        @yield('content-css')
    </head>
    <body>
        @yield('header')
        @yield('content')
        @yield('footer')

        <!-- JQUERY -->
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" crossorigin="anonymous"></script>
        <!-- BOOTSTRAP JS -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
        @yield('content-js')
    </body>
</html>

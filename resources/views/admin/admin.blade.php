@extends('admin.base')

@section('title', 'Victoria - Admin')

@section('header')
	@include('admin._header')
@endsection

@section('content')
<div class="container-fluid admin-content">
  <div class="row">

		@include('admin._sidebar')

    <div class="col">
      <p class="titulos" style="text-align: center;">FERIAS VICTORIA</p>
    </div>

  </div>
</div>
@endsection

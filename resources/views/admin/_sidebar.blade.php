<div class="col-md-3 col-xl-2 admin-sidebar">


  <nav class="navbar navbar-expand-lg">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent2" aria-controls="navbarSupportedContent2" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent2">
      <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link active" href="/feria"><i class="fas fa-chevron-circle-right"></i>&nbsp;Ferias</a>
          <div class="dropdown-divider"></div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"><i class="fas fa-chevron-circle-right"></i>&nbsp;Palenque</a>
          <div class="dropdown-divider"></div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"><i class="fas fa-chevron-circle-right"></i>&nbsp;Experiencias Victoria</a>
        </li>
      </ul>
    </div>
  </nav>

</div>

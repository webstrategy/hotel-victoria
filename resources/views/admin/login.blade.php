@extends('admin.base')

@section('content')

<div class="container-fluid login-bg">
  <div class="row justify-content-md-center align-items-center" style="height:100%;">

    <div class="col-sm-12 col-md-3 col-lg-3">
      <div class="bd-example">

        <div class="card login-card">
          <div class="card-body">
            <p class="titulos" style="text-align: center;">Login Victoria</p>

            <form class="form form-horizontal" role="form" method="POST" action="admin-auth">
              {{ csrf_field() }}
              <div class="form-group row">
                <div class="col">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text login-input-pre"><i class="fas fa-envelope"></i></div>
                    </div>
                    <input type="text" class="form-control login-form-btn" id="admin-email" name="email" placeholder="Correo">
                  </div>
                  <small class="text-warning">{{ $errors->first('email') }}</small>
                </div>
              </div>
              <div class="form-group row">
                <div class="col">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text login-input-pre"><i class="fas fa-lock"></i></div>
                    </div>
                    <input type="password" class="form-control login-form-btn" id="admin-pass" name="password" placeholder="Contraseña">
                  </div>
                  <small class="text-warning">{{ $errors->first('password') }}</small>
                </div>
              </div>
              <div class="form-group row">
                <div class="col text-center">
                  <button type="submit" class="btn login-btn">Iniciar Sesión</button>
                </div>
              </div>
            </form>
          </div>
        </div>

      </div>
    </div>

  </div>
</div>
@endsection

@extends('base')

<div class="col-md-12 registro">
      <div class="row"align="center">
        <div class="registro_logo"><br/><img src="images/logo_victoria.png" alt=""></div>
      </div>

      <div class="row"align="center">
          <div class="txt_titulo_registo" style="margin-top:100px;">¿ERES MAYOR  <br/>DE EDAD?</div>
          <p> Para poder entrar a este sitio debes tener más de 18 años.<br/>Por favor, verifica tu edad.</p>

          <input class="caja_registro" type="text" name="DÍA" maxlength="2" placeholder="DÍA">
          <input class="caja_registro" type="text" name="MES" maxlength="2" placeholder="MES">
          <input class="caja_registro" type="text" name="AÑO" maxlength="4" placeholder="AÑO">
      </div>

    <div class="row" align="center" style="margin-top:15px;">
      <input class="boton_registro"type="button" value="ENTRAR" id="boton">
      <p><u>O INGRESA CON FACEBOOK</u></p>
    </div>

    <div class="row" align="center" style="margin-top:120px;">
      <div class="txt_leg">TODO CON MEDIDA <br>PATROCINADO POR VICTORIA</div>
      <div class="txt_opacity" style="margin-top:30px;" >
        <a style="padding-right: 20px;" href="https://www.gmodelo.mx/terminos.html" target="_blank">Términos y condiciones</a>
        <a href="https://www.gmodelo.mx/politicas.html" target="_blank">Políticas de privacidad</a>
      </div>
    </div>
  </div>

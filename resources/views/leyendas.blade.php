@extends('base')

@section('metakey', ', leyendas, hotel victoria.')

@section('title', 'Hotel Victoria - Leyendas')
@section('active-home', 'active')

@section('header')
	@include('partials._header')
@endsection

@section('content')

<div class="container-fluid ">

  <!-- <div class="row" style="margin-left: -15px; margin-right: -15px;">
    <div class="leyendas">
      <div class="col-md-offset-2 col-md-8 hidden-sm hidden-xs" align="center">
        <div class="txt_opacity" style="margin-top:60px">Ahora en  Hotel Victoria</div>
        <div class="titulos_ley hidden-xs">LA PAS<u style="text-decoration: none; border-bottom: 5px solid #ea002a; padding-bottom:7px;">CU</u>ALITA</div>
        <div class="txt_ley hidden-xs">Cuenta la historia que la picadura de un alacrán terminó con la vida de una joven, en el día de su boda.
          Algunos creen que su madre, Doña Pascualita, embalsamó el cadaver y lo puso como maniquí en su tienda de vestidos de novia.
          Muchos aseguran que la han visto moverse, llorar o cambiar de expresión. ¿Es un maniquí hiperrealista o un cadáver embalsamado?
          Descúbrelo en la visita de cualquiera de nuestras leyendas en Hotel Victoria. La Pascualita estará en exhibición para todos los asistentes
        </div>
				<div class="txt_opacity" style="margin-top:40px">*EVENTO PARA MAYORES DE EDAD</div>
				</div>

			<div class="col-sm-12 col-xs-12 visible-sm visible-xs" align="center">
				<div class="txt_opacity" style="margin-top:20px">Ahora en  Hotel Victoria</div>
        <div class="titulos_ley_m">LA PAS<u style="text-decoration: none; border-bottom: 5px solid #ea002a; padding-bottom:7px;">CU</u>ALITA</div>
        <div class="txt_ley_m ">Cuenta la historia que la picadura de un alacrán terminó con la vida de una joven, en el día de su boda.
          Algunos creen que su madre, Doña Pascualita, embalsamó el cadaver y lo puso como maniquí en su tienda de vestidos de novia.
          Muchos aseguran que la han visto moverse, llorar o cambiar de expresión. ¿Es un maniquí hiperrealista o un cadáver embalsamado?
          Descúbrelo en la visita de cualquiera de nuestras leyendas en Hotel Victoria. La Pascualita estará en exhibición para todos los asistentes
        </div>
        <div class="txt_opacity" style="margin-top:40px">*EVENTO PARA MAYORES DE EDAD</div>
      </div>
    </div>
  </div> -->


   <div class="row" style="padding-top:50px">
      <div class="col-md-offset-2 col-md-8 col-sm-12 col-xs-12">
        <div class="titulos"><u style="text-decoration: none; border-bottom: 5px solid #ea002a; padding-bottom:7px;">LE</u>YENDAS</div>
      </div>
    </div>

    <!-- <div class="row" style="padding-top:50px">
      <div class="col-md-offset-2 col-md-4 col-sm-6 col-xs-12" align="left">
        <div class="txt_boletos">Biscuit soufflé toffee chupa chups brownie sesame snaps. Sesame snaps carrot cake soufflé dragée marshmallow carrot cake tart sugar plum chocolate cake. Liquorice icing gingerbread cupcake apple pie pastry toffee jujubes. Dessert sweet icin.
          Tart halvah toffee bonbon muffin brownie. Dragée sugar plum cookie sweet chupa chups pie fruitcake candy.
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12 compra-boletos" align="center" >
      <a href="boletos"><input class="boton_boletos"type="button" value="COMPRA TUS BOLETOS" id="boton"></a>
      </div>
    </div> -->

		<div class="col-md-12 col-sm-12 col-xs-12" align="center" >
			<a href="boletos"><input class="boton_boletos"type="button" value="COMPRA TUS BOLETOS" id="boton"></a>
		</div>

    <<div class="row" style="padding-top:30px;">
      <div class="col-md-offset-1 col-md-10 separador">&nbsp;</div>
    </div>


    <div class="row" style="padding-top:30px">
      <div class="col-md-offset-2 col-md-4 col-sm-5 col-xs-12" align="left">
        <img class="img-responsive" style="padding:20px" src="images/hv_ley_zac.jpg" alt="">
      </div>
      <div class="col-md-3 col-sm-5 col-xs-12" align="left">
        <div class="txt_boletos_til2"> LA ZACATECANA</div>
          <div class="txt_boletos">Hace mucho en Zacatecas vivía una pareja de esposos, el señor se dedicaba a la minería por lo cual su fortuna era muy grande, lo que la avaricia se apodero de su mujer, ella lo mandó a matar y luego asesinó al victimario. Días después ella murió de forma trágica.<br><br>
            Desde ese día quien viva en esa casa tendrá infortunios que terminarán con la vida de sus habitantes.
          </div>
          <!-- <a href="ley_int.php"><div class="txt_rojo2">Saber más</div></a> -->
      </div>
    </div>
    <div class="row">
      <div class="col-md-offset-1 col-md-10 separador">&nbsp;</div>
    </div>

    <div class="row" style="padding-top:30px;">
      <div class="col-md-offset-2 col-md-4 col-sm-5 col-xs-12" align="left">
        <img class="img-responsive" style="padding:20px" src="images/hv_ley_rel.jpg" alt="">
      </div>
      <div class="col-md-3 col-sm-5 col-xs-12" align="left">
        <div class="txt_boletos_til2">EL RELOJERO</div>
          <div class="txt_boletos">Hace mucho tiempo había una señor que junto con su esposa tenían una relojería que era la mejor del país, un día un comerciante muy rico y poderoso se presentó es su tienda para arreglar su reloj de oro, el regresaba con la excusa de que su reloj no servía pero la realidad era que le gustaba la esposa del relojero, al final el comerciante mató al relojero para quedarse con su esposa pero ella no quiso. Así todas las mañanas el cuerpo del relojero se presentaba en la casa del comerciante junto con el reloj de oro.<br><br>
          Cuenta la leyenda que la tumba del relojero fue profanada y su esposa desapareció junto con el reloj de oro.
          </div>
          <!-- <a href="#"><div class="txt_rojo2">Saber más</div></a> -->
      </div>
    </div>
    <div class="row">
      <div class="col-md-offset-1 col-md-10 separador">&nbsp;</div>
    </div>


    <div class="row" style="padding-top:30px;">
      <div class="col-md-offset-2 col-md-4 col-sm-5 col-xs-12" align="left">
        <img class="img-responsive" style="padding:20px" src="images/hv_ley_pla.jpg" alt="">
      </div>
      <div class="col-md-3 col-sm-5 col-xs-12" align="left">
        <div class="txt_boletos_til2">LA PLANCHADA</div>
          <div class="txt_boletos">Eulalia, una enfermera que siempre vestía pulcramente y era dedicada en su trabajo, se enamoró del doctor equivocado, cuando este la decepcionó, ella perdió todo interés por la vida hasta el punto de morir en el Hospital Juárez de la Ciudad de México.<br><br>
          Hoy en día algunos pacientes y enfermeras dicen que Eulalia sigue cubriendo algunos turnos por la noche, siempre portando su uniforme perfectamente limpio y planchado.
          </div>
          <!-- <a href="#"><div class="txt_rojo2">Saber más</div></a> -->
      </div>
    </div>
    <div class="row">
      <div class="col-md-offset-1 col-md-10 separador">&nbsp;</div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12" align="center" >
      <a href="boletos"><input class="boton_boletos"type="button" value="COMPRA TUS BOLETOS" id="boton"></a>
    </div>




</div>
@endsection

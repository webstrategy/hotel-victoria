@extends('base')

@section('metakey', ', hotel victoria.')

@section('title', 'Hotel Victoria - Bienvenido')
@section('active-home', 'active')

@section('header')
	@include('partials._header')
@endsection

@section('content')
<script>


// function ocultarvideo(video){
// 	if(video==1){
// 		document.getElementById("imgvideo").style.display ='none';
// 		document.getElementById("videodesk").style.visibility = 'visible';
// 	}
// }
// 		$( "#imgvideo" ).click(function() {
//   alert( "Handler for .click() called." );
// });
</script>
<div class="container-fluid espacio-nav">
<div class="row">
    <div class=" col-lg-12 col-md-12 hidden-sm hidden-xs hotel">
				<ul class="iconos">
					<li><a href="https://www.facebook.com/CervezaVictoriaOficial/" target="_blank"><img style="padding-top:50px;" src="images/hv_fb.png"></a></li>
					<li><a href="https://www.youtube.com/user/CervezaVictoriaMx" target="_blank"><img style="padding-top:20px;" src="images/hv_yt.png" alt=""></a></li>
					<li><a href="https://twitter.com/VictoriaMx_?lang=es" target="_blank"><img style="padding-top:20px;" src="images/hv_tw.png" alt=""></a></li>
					<li><a href="https://www.instagram.com/cerveza.victoria/?hl=es" target="_blank"><img style="padding-top:20px;" src="images/hv_in.png" alt=""></a></li>
					<!-- <li><a href="#" target="_blank"><img src="images/hv_rs.png" alt=""></a></li> -->
				</ul>
      <!-- <iframe width="100%" height="680" src="https://www.youtube.com/embed/KSWgdSFBmRM" frameborder="0" allowfullscreen></iframe> -->
			<!-- <div id="imgvideo">
				<img onclick="javascript:ocultarvideo(1)" src="images/hotel_victoria.jpg" class="img-responsive" />
			</div> -->
			<div id="videodesk" class="col-md-12 visible-lg visible-md hidden-sm hidden-xs hotel">
				<video width="100%" height="550" poster="images/hotel_victoria.png" controls class="visible-lg visible-md hidden-sm hidden-xs">
					<source src="videos/TRAILER_HOTEL_VICTORIA_V169.mp4" type="video/mp4">
				</video>
			</div>

    </div>

		<div class="col-md-12 visible-sm visible-xs hidden-lg hidden-md hotel">
			<video width="100%" height="100%" poster="images/hotel_victoria2.jpg" controls class="visible-sm visible-xs hidden-lg hidden-md">
				 <source src="videos/TRAILER_HOTEL_VICTORIA_V1-1.mp4" type="video/mp4">
			</video>
		</div>


    <!-- <img class="img-responsive" src="images/hotel_victoria.jpg" alt="Hotel Victoria"> -->
  </div>

  <div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12" align="center" >
			<a href="boletos"><button class="boton_le">ÚLTIMAS FECHAS DISPONIBLES</button></a>

    </div>

      <div class="col-md-offset-1 col-md-5 col-sm-6 col-xs-12" style="margin-top:50px">
        <div class="titulos">
          ¿TE ATREVES A <br> <u style="text-decoration: none; border-bottom: 5px solid #ea002a; padding-bottom:0px;">EN</u>TRAR A ESTE HOTEL?
        </div>
        <div class="txt_int">
					Victoria rescata las leyendas tradicionales mexicanas a través de una experiencia multisensorial que te llevará a vivir paso a paso suspenso, terror y adrenalina a niveles inimaginables, donde las
					emociones extremas y los gritos serán tus acompañantes. <br><br>
					Visita este enigmático Hotel y revive en carne propia esas leyendas que caracterizan a México, te aseguramos que tu estancia sera placentera.
          <!-- See The Future.” Is it an advertising slogan for sunglasses or an invitation to glimpse what’s to come? We will never tell, but the avant-garde and mysterious new campaign for Capitol Couture Eyewear has everyone whispering about the potential clairvoyant power of these bold accessories.<br><br>
          The arresting billboards—which feature a sepia and white houndstooth motif and a male model clad in the classic pattern and mirrored shades—appeared overnight in the Capitol  and quickly caused rubbernecking.  (Non-Panem residents in Los Angeles can see the giant ads on various intersections throughout the city, including Crenshaw Boulevard & Country Club Drive. In Manhattan, the billboards are in different locations such as Centre Street & Grand Street.) -->
          </p>
        </div>
      </div>

        <div class="col-md-offset-1 col-md-4 col-sm-6 col-xs-12" style="margin-top:50px" align="center">
          <img class="img-responsive" src="images/hotel_ojo.jpg" alt="">
          <a href="leyendas"><button class="boton_ley"><img  src="images/hv_icono_ver.png" alt="" />&nbsp;&nbsp;|&nbsp;&nbsp;VER LEYENDAS</button></a>
        </div>
  </div>


    <div class="row" style="margin-top:50px">
      <div class="testimonios col-md-12 hidden-xs">
        <div class="testimonio">
            <div class="testi col-md-offset-2 col-md-8"> Testimonios
          <div class="txt_testimonio">
              “Es una gran experiencia, sabes que sucederá algo pero no sabes qué, vas de sorpresa en sorpresa.”
            </div>
            <strong>Hijo del Santo</strong>
           </div>
        </div>
      </div>
<!-- MOBILE -->
      <div class="col-xs-12 col-sm-12 testimonios visible-xs" style="margin-top:50px">
        <div class="testimonio_m">
            <div class="testi_m"> Testimonios
            <div class="txt_testimonio_m">
              “Es una gran experiencia, sabes que sucederá algo pero no sabes qué, vas de sorpresa en sorpresa.”
            </div>
          <strong>Hijo del Santo</strong>
           </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-offset-2 col-md-8" align="center">
            <div class="titulos"><u style="text-decoration: none; border-bottom: 5px solid #ea002a; padding-bottom:5px;">#H</u>otelVictoria</div>

            <div id="selected-txt" class="txt_int">Compártenos tu experiencia usando #HotelVictoria</div>
            <!-- <img id="selected-img" class="img-responsive" src="images/hv_insta.jpg" alt=""> -->
      </div>
  </div>

  <div class="row">
    <div class="col-md-offset-2 col-md-8" align="center" style="margin-top:50px">
          <img class="img-responsive" src="images/hv_caballo.jpg" alt="">
    </div>
  </div>



</div>

@include('partials._login')

@endsection
@section('content-js')
<script type="text/javascript" src="js/controllers/login.js"></script>
@endsection

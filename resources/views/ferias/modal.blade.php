<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-body">
        <div class="left">
          <img src="{{ asset('ferias/images/logo-victoria.svg') }}" alt="">
        </div>
        <div class="right">
          <form action="">
            <p>
              Para poder entrar a este sitio debes tener más de 18 años.
              <br>Por favor, verifica tu edad.
            </p>
            <div class="form-group">
              <input id="day_field" class="caja_registro" type="text" name="DÍA" min="1" max="31" placeholder="DÍA">
              <input id="month_field" class="caja_registro" type="text" name="MES" min="1" max="12" placeholder="MES">
              <input id="year_field" class="caja_registro" type="text" maxlength="4" name="AÑO" placeholder="AÑO">
            </div>

            <button type="submit">
              <i class="fas fa-sign-out-alt"></i>
              INGRESAR
            </button>

            <h3> Ó </h3>

            <button type="submit" class="facebook">
              <i class="fab fa-facebook-square"></i>
              INGRESA CON FACEBOOK
            </button>

            <div class="avisos">
              <a href="#">Términos y condiciones</a>
              <a href="#">Políticas de privacidad</a>
            </div>

          </form>
        </div>
      </div>

    </div>
  </div>
</div>
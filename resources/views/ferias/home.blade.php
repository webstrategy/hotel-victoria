@extends('ferias.layout')


@section('content')

	@include('ferias.components.menu')

	<div class="container-menu-home">
		<nav id="menu">
			<ul>
				<li>
					<a href="{{ url('/f-home') }}">
						<img src="{{ asset('ferias/images/logo-patrocinado.svg') }}" alt="">
					</a>
				</li>
				<li id="menu-icon">
					<i class="fas fa-bars"></i>
				</li>
			</ul>
		</nav>
	</div>

	<div class="container-home">
		<a href="{{ url('/f-home2') }}" class="left">
			<img src="{{ asset('ferias/images/logo-patrocinado.svg') }}" alt="">
		</a>
		<a href="{{ url('/f-home2') }}" class="right"></a>

		<div class="aviso">
			<h2>TODO CON MEDIDA</h2>
			<div class="links">
				<a href="#">Términos y condiciones</a>
				<a href="#">Aviso de privacidad</a>
			</div>
		</div>
	</div>

	@include('ferias.modal')

@endsection
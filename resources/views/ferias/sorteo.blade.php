@extends('ferias.layout')

@section('content')

	@include('ferias.components.menu')

	<div class="container-sorteo">
		<nav id="menu">
			<ul>
				<li>
					<a href="#">
						<img src="{{ asset('ferias/images/logo-patrocinado.svg') }}" alt="">
					</a>
				</li>
				<li id="menu-icon">
					<i class="fas fa-bars"></i>
				</li>
			</ul>
		</nav>
		
		<div class="text-center">
			<img class="img-fluid logo-feria" src="{{ asset('ferias/images/logo_ferias_victoria.svg') }}" alt="">
		</div>

		<div class="content">
			<h1>Mecánica de sorteo</h1>
			<h2>Lorem ipsum dolor sit amet consectetur adipisicing elit.</h2>

			<h3>Pregunta uno</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi, expedita, iste, impedit corporis aperiam temporibus non, voluptate ullam quae ipsam perferendis enim quod quo rem odit molestias reprehenderit incidunt modi?
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim recusandae natus aperiam accusantium dicta dolore non beatae sint illum, dolorum magnam fuga vitae debitis repudiandae perspiciatis, necessitatibus, qui, esse soluta!</p>
			
			<h3>Pregunta dos</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi, expedita, iste, impedit corporis aperiam temporibus non, voluptate ullam quae ipsam perferendis enim quod quo rem odit molestias reprehenderit incidunt modi?
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim recusandae natus aperiam accusantium dicta dolore non beatae sint illum, dolorum magnam fuga vitae debitis repudiandae perspiciatis, necessitatibus, qui, esse soluta!</p>

			<h3>Pregunta tres</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi, expedita, iste, impedit corporis aperiam temporibus non, voluptate ullam quae ipsam perferendis enim quod quo rem odit molestias reprehenderit incidunt modi?
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim recusandae natus aperiam accusantium dicta dolore non beatae sint illum, dolorum magnam fuga vitae debitis repudiandae perspiciatis, necessitatibus, qui, esse soluta!</p>

			<h3>Pregunta cuatro</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi, expedita, iste, impedit corporis aperiam temporibus non, voluptate ullam quae ipsam perferendis enim quod quo rem odit molestias reprehenderit incidunt modi?
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim recusandae natus aperiam accusantium dicta dolore non beatae sint illum, dolorum magnam fuga vitae debitis repudiandae perspiciatis, necessitatibus, qui, esse soluta!</p>

		</div>

		
		<div class="container-ferias ">
			<footer>
				<p id="medida" class="wd">todo con medida</p>
				<ul id="icons" class="wd">
					<li>
						<a href="">
							<i class="fab fa-twitter"></i>
						</a>
					</li>
					<li>
						<a href="">
							<i class="fab fa-facebook-f"></i>
						</a>
					</li>
					<li>
						<a href="">
							<i class="fab fa-instagram"></i>
						</a>
					</li>
					<li>
						<a href="">
							<i class="fab fa-youtube"></i>
						</a>
					</li>
				</ul>
				<ul id="terminos" class="wd">
					<li>
						<a href="">
							Términos y condiciones
						</a>
					</li>
					<li>
						<a href="">
							Aviso de privacidad
						</a>
					</li>
				</ul>
			</footer>
		</div>
		

	</div>



@endsection
@extends('ferias.layout')


@section('content')

	@include('ferias.components.menu')

	<div class="container-landing">
		<nav id="menu">
			<ul>
				<li>
					<a href="{{ url('/f-home') }}">
						<img src="{{ asset('ferias/images/logo-patrocinado.svg') }}" alt="">
					</a>
				</li>
				<li id="menu-icon">
					<i class="fas fa-bars"></i>
				</li>
			</ul>
		</nav>

		<div class="slider">
			<div class="slider-content">
				<div class="wrapper-slider">
					<img src="{{ asset('ferias/images/back-slider.png') }}" alt="" class="img-fluid">
					<div class="content">
						<div class="left">
							<img src="{{ asset('ferias/images/logo-feria-caballos.svg') }}" alt="">
						</div>
						<div class="right">
							<h1>feria internacional de caballo texcoco</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil blanditiis fuga necessitatibus quam veniam odit commodi hic temporibus? Voluptates nesciunt accusamus natus placeat maxime iusto architecto quisquam! Est, quaerat, corrupti.</p>
						</div>
					</div>
				</div>	
			</div>
		</div>
	
		<div class="artistas">
			
			<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
				<li class="nav-item">
					<a class="nav-link" id="palenque-tab" data-toggle="pill" href="#palenque" role="tab" aria-controls="palenque">
						PALENQUE
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link active" id="teatro-tab" data-toggle="pill" href="#teatro" role="tab" aria-controls="teatro">
						TEATRO DEL PUEBLO
					</a>
				</li>
			</ul>

			<div class="tab-content" id="pills-tabContent">
				<div class="tab-pane" id="palenque" role="tabpanel" aria-labelledby="palenque-tab">
					
					<div class="slick1">
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/tigres.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/foto.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/foto.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/foto.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/foto.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/foto.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/foto.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/foto.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/foto.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/foto.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
					</div>

				</div>
				<div class="tab-pane show active" id="teatro" role="tabpanel" aria-labelledby="teatro-tab">
					
					<div class="slick2">
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/foto.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/tigres.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/tigres.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/tigres.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/tigres.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/tigres.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/tigres.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/tigres.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/tigres.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
				        <div class="item">
				            <img class="foto" src="{{ asset('ferias/images/tigres.jpg') }}" alt="" class="img-fluid"/>
				            <div class="desc">
				            	<h3>Los Tigres del Norte</h3>
				            	<p>16 de Mayo</p>
				            </div>
				            <div class="shadow"></div>
				            <div class="logo">
				            	<img src="{{ asset('ferias/images/v_victoria.svg') }}" alt=""/>
				            </div>
				        </div>
					</div>

				</div>
			</div><!-- tab content -->
		</div>

		<div class="post-content">
			<div class="wrapper-posts">
				<h2 class="title-post">
					vive la cultura
				</h2>
				<div class="wrapper-post">
					<div class="post">
						<a href="{{ url('/post') }}">
							<img src="{{ asset('ferias/images/post1.jpg') }}" alt="" class="img-fluid">
						</a>
						
						<div class="desc">
							<a href="{{ url('/post') }}">
								<h2>Title Post</h2>
							</a>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae tempore aliquam quam repellat quibusdam explicabo deleniti architecto voluptatem ab eligendi numquam natus quaerat vitae ea tenetur, totam, ducimus fuga. Et!
							</p>
							<a class="button" href="{{ url('/post') }}">
								MÁS INFORMACIÓN
							</a>		
						</div>
					</div>
					<div class="post">
						<a href="{{ url('/post') }}">
							<img src="{{ asset('ferias/images/post1.jpg') }}" alt="" class="img-fluid">
						</a>
						
						<div class="desc">
							<a href="{{ url('/post') }}">
								<h2>Title Post</h2>
							</a>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae tempore aliquam quam repellat quibusdam explicabo deleniti architecto voluptatem ab eligendi numquam natus quaerat vitae ea tenetur, totam, ducimus fuga. Et!
							</p>
							<a class="button" href="{{ url('/post') }}">
								MÁS INFORMACIÓN
							</a>		
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="palenque-chingon">
			<div class="wrapper-palenque">
				<h2 class="title1">PALENQUE CHINGON</h2>
				<h2 class="title2">Vive un palenque chingon con Victoria</h2>
				<div class="table-palenque">
					<div class="paquete">
						<h2 class="title">
							Palenque Victoria
						</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium ratione labore earum blanditiis magnam velit unde saepe at rem, nobis itaque voluptatem numquam delectus magni, autem repellat odio nemo impedit!</p>
						<a href="#">
							COMPRA TUS BOLETOS
						</a>
					</div>
					<div class="paquete">
						<h2 class="title">
							Palenque Victoria + Precopeo Chingon
						</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium ratione labore earum blanditiis magnam velit unde saepe at rem, nobis itaque voluptatem numquam delectus magni, autem repellat odio nemo impedit!</p>
						<a href="#">
							COMPRA TUS BOLETOS
						</a>
					</div>
					<div class="paquete">
						<h2 class="title">
							Palenque Victoria + Precopeo Chingon
						</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium ratione labore earum blanditiis magnam velit unde saepe at rem, nobis itaque voluptatem numquam delectus magni, autem repellat odio nemo impedit!</p>
						<a href="#" class="btn-payment">
							COMPRA TUS BOLETOS
						</a>
					</div>

					<div class="paquete">
						<h2 class="title">
							Sesiones Mestizas
						</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium ratione labore earum blanditiis magnam velit unde saepe at rem, nobis itaque voluptatem numquam delectus magni, autem repellat odio nemo impedit!</p>
						<a href="#" class="btn-register">
							REGISTRATE Y GANA
						</a>
					</div>
				</div>
			</div>
			
			<div class="my-modal">
				<div class="modal__header">
					<h4>regístrate</h4>
					<i class="far fa-times-circle close-modal"></i>
				</div>
				<form class="modal__content">
						<div class="form-group-register">
							<input type="text" placeholder="Nombre">
							<input type="text" placeholder="Apellidos">
							<input type="text" placeholder="Celular">
							<input type="text" placeholder="C.P.">
							<input type="text" placeholder="Estado">
							<input type="text" placeholder="Ciudad">
							<input type="text" placeholder="Colonia">
							<input type="text" placeholder="Facebook">
						</div>

						<div class="checkbox-modal">
							<div class="form-check">
								<input type="checkbox" class="form-check-input" id="exampleCheck1">
								<label class="form-check-label" for="exampleCheck1">Términos y Condiciones</label>
							</div>

							<div class="form-check">
								<input type="checkbox" class="form-check-input" id="exampleCheck2">
								<label class="form-check-label" for="exampleCheck2">Bases de concurso</label>
							</div>
						</div>

						<div class="buttons-modal-register">
							<button type="submit">Guardar</button>
							<button class="close-modal">Cancelar</button>
						</div>
				</form>
				<div class="modal__footer"></div>
			</div>

			<div class="my-modal-payment">
				<div class="modal__header">
					<h4>datos de pago</h4>
					<i class="far fa-times-circle close-modal"></i>
				</div>
				<form class="modal__content">
						<div class="form-group-register">
							<input type="text" placeholder="Nombre">
							<input type="text" placeholder="Apellidos">
							<input type="text" placeholder="Correo Electrónico">
							<!-- <input type="text" placeholder="C.P."> -->
							<input type="text" placeholder="Estado">
							<input type="text" placeholder="Ciudad">
							<input type="text" placeholder="Colonia">
							<input type="text" placeholder="Número de Tarjeta">

							<div class="inputs-card">
								<input type="text" placeholder="Fecha Vencimiento">
								<input type="text" placeholder="Código de Seguridad">
							</div>
						</div>



						<div class="checkbox-modal">
							<div class="form-check">
								<input type="checkbox" class="form-check-input" id="exampleCheck1">
								<label class="form-check-label" for="exampleCheck1">He leído y acepto términos y condiciones</label>
							</div>
						</div>

						<div class="buttons-modal-register">
							<button type="submit">Realizar Pago</button>
						</div>
				</form>
				<div class="modal__footer"></div>
			</div>

		</div>

		<div class="mapa">
			<div class="header">
				<h2>HOT SPOTS</h2>
				<a href="#">DESCARGAR MAPA</a>
			</div>
			<img src="{{ asset('ferias/images/hot_spots.jpg') }}" alt="">
		</div>

		<div class="social-content">
			<div class="wrapper-content">
				<h2>SOCIAL</h2>
				<div class="slick3">
			        <div class="item">
			            <img class="foto" src="{{ asset('ferias/images/foto.jpg') }}" alt="" class="img-fluid"/>
			            <div class="desc">
			            	<h3>Los Tigres del Norte</h3>
			            	<p>16 de Mayo</p>
			            </div>
			            <div class="shadow"></div>
			        </div>
			        <div class="item">
			            <img class="foto" src="{{ asset('ferias/images/tigres.jpg') }}" alt="" class="img-fluid"/>
			            <div class="desc">
			            	<h3>Los Tigres del Norte</h3>
			            	<p>16 de Mayo</p>
			            </div>
			            <div class="shadow"></div>
			        </div>
			        <div class="item">
			            <img class="foto" src="{{ asset('ferias/images/tigres.jpg') }}" alt="" class="img-fluid"/>
			            <div class="desc">
			            	<h3>Los Tigres del Norte</h3>
			            	<p>16 de Mayo</p>
			            </div>
			            <div class="shadow"></div>
			        </div>
			        <div class="item">
			            <img class="foto" src="{{ asset('ferias/images/tigres.jpg') }}" alt="" class="img-fluid"/>
			            <div class="desc">
			            	<h3>Los Tigres del Norte</h3>
			            	<p>16 de Mayo</p>
			            </div>
			            <div class="shadow"></div>
			        </div>
				</div>
			</div>
		</div>

		<div class="ubicacion">
			<h2>UBICACIÓN</h2>
			<div id="map"></div>
		</div>

		<div class="container-ferias ">
			<footer>
				<p id="medida" class="wd">todo con medida</p>
				<ul id="icons" class="wd">
					<li>
						<a href="">
							<i class="fab fa-twitter"></i>
						</a>
					</li>
					<li>
						<a href="">
							<i class="fab fa-facebook-f"></i>
						</a>
					</li>
					<li>
						<a href="">
							<i class="fab fa-instagram"></i>
						</a>
					</li>
					<li>
						<a href="">
							<i class="fab fa-youtube"></i>
						</a>
					</li>
				</ul>
				<ul id="terminos" class="wd">
					<li>
						<a href="">
							Términos y condiciones
						</a>
					</li>
					<li>
						<a href="">
							Aviso de privacidad
						</a>
					</li>
				</ul>
			</footer>
		</div>



@endsection














@extends('ferias.layout')

@section('content')

	@include('ferias.components.menu')

	<div class="container-detail-post">
		<nav id="menu">
			<ul>
				<li>
					<a href="#">
						<img src="{{ asset('ferias/images/logo-patrocinado.svg') }}" alt="">
					</a>
				</li>
				<li id="menu-icon">
					<i class="fas fa-bars"></i>
				</li>
			</ul>
		</nav>
	
		<div class="wrapper-detail-post">

			<div class="title-arrow">
				<a href="{{ url('/landing') }}">
					<i class="fas fa-angle-left"></i>
				</a>
				<h2>VIVIE LA CULTURA</h2>
			</div>

			<a class="subtitle" href="">Molino de flores a caballo</a>

			<div class="content">
				<div class="image-portada">
					<img src="{{ asset('ferias/images/post1.jpg') }}" alt="" class="img-fluid">
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos facere voluptatum pariatur at, accusantium, quisquam placeat mollitia, dolor sunt ipsa vero quo quibusdam, deleniti quis delectus. Labore rerum, praesentium nobis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus cumque vero blanditiis quos eos ipsum nihil alias qui excepturi, aspernatur iusto voluptatum maxime quibusdam voluptas dignissimos numquam hic aperiam enim.
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero maiores expedita quisquam, asperiores perspiciatis quo dicta quae cupiditate consequuntur, quos eum incidunt eveniet. Itaque magnam molestiae veniam consequatur beatae dolorum.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est ea eligendi fuga illum sequi ut, cumque cupiditate, nostrum expedita similique asperiores ipsam temporibus voluptate vitae sed, deserunt. Adipisci iusto, commodi.</p>
				
				<div class="content2">
					<div class="text-left">
						<h3>¿QUÉ HAREMOS?</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi ipsam dolor, laborum quos, nam alias iure velit temporibus illo debitis sint deserunt repellat facere cumque id animi! Quos, natus, nam!</p>

						<h3>¿QUÉ INCLUYE?</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi ipsam dolor, laborum quos, nam alias iure velit temporibus illo debitis sint deserunt repellat facere cumque id animi! Quos, natus, nam! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates corrupti laudantium, adipisci nulla alias delectus optio, eos culpa repudiandae, eveniet sed nobis tempore quaerat doloribus molestias neque ea fugiat consequatur!</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt atque qui reprehenderit iusto, quibusdam excepturi ducimus earum tempore laudantium explicabo provident nemo fugiat soluta vero, voluptatibus quos similique dolores quas.</p>
					</div>

					<div class="requisitos">
						<a href="" class="btn-comprar">COMPRAR</a>
						<h3>REQUISITOS</h3>
						<ul>
							<li>- Lorem ipsum dolor sit</li>
							<li>- consectetur adipisicing elit</li>
							<li>- Quasi ipsam dolor, laborum quos</li>
							<li>- Lorem ipsum dolor sit amet</li>
							<li>- laudantium explicabo provident</li>
						</ul>
					</div>
				</div>
			</div>

		</div>

		<div id="map"></div>

		<div class="container-ferias ">
			<footer>
				<p id="medida" class="wd">todo con medida</p>
				<ul id="icons" class="wd">
					<li>
						<a href="">
							<i class="fab fa-twitter"></i>
						</a>
					</li>
					<li>
						<a href="">
							<i class="fab fa-facebook-f"></i>
						</a>
					</li>
					<li>
						<a href="">
							<i class="fab fa-instagram"></i>
						</a>
					</li>
					<li>
						<a href="">
							<i class="fab fa-youtube"></i>
						</a>
					</li>
				</ul>
				<ul id="terminos" class="wd">
					<li>
						<a href="">
							Términos y condiciones
						</a>
					</li>
					<li>
						<a href="">
							Aviso de privacidad
						</a>
					</li>
				</ul>
			</footer>
		</div>

	</div>



@endsection
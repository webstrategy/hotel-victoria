@extends('ferias.layout')

@section('content')

	@include('ferias.components.menu')

	<div class="container-ferias">
		<nav id="menu">
			<ul>
				<li>
					<a href="{{ url('/f-home') }}">
						<img src="{{ asset('ferias/images/logo-patrocinado.svg') }}" alt="">
					</a>
				</li>
				<li id="menu-icon">
					<i class="fas fa-bars"></i>
				</li>
			</ul>
		</nav>

		<div class="center-box">
			<img id="top" class="img-fluid" src="{{ asset('ferias/images/logo_ferias_victoria.svg') }}" alt="">
			<div class="desc1">
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis magni reprehenderit, id laudantium voluptas necessitatibus quibusdam earum dignissimos totam soluta ut nam voluptates inventore, enim, nisi, quisquam! Minus, excepturi vitae!
				</p>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis magni reprehenderit, id laudantium voluptas necessitatibus quibusdam earum dignissimos totam soluta ut nam voluptates inventore, enim, nisi, quisquam! Minus, excepturi vitae!
				</p>
			</div>
			<div class="ferias-box">
				<a href="{{ url('/landing') }}" class="left">
					<p>
						feria internacional del caballo texcoco
					</p>
					<img src="{{ asset('ferias/images/logo-feria-caballos.svg') }}" alt="">
				</a>
				<a href="{{ url('/landing') }}" class="right">
					<p>
						feria estatal del león
					</p>
					<img src="{{ asset('ferias/images/logo-feria-caballos.svg') }}" alt="">
				</a>
			</div>
		</div> <!-- fin center-box -->

		<footer>
			<p id="medida" class="wd">todo con medida</p>
			<ul id="icons" class="wd">
				<li>
					<a href="">
						<i class="fab fa-twitter"></i>
					</a>
				</li>
				<li>
					<a href="">
						<i class="fab fa-facebook-f"></i>
					</a>
				</li>
				<li>
					<a href="">
						<i class="fab fa-instagram"></i>
					</a>
				</li>
				<li>
					<a href="">
						<i class="fab fa-youtube"></i>
					</a>
				</li>
			</ul>
			<ul id="terminos" class="wd">
				<li>
					<a href="">
						Términos y condiciones
					</a>
				</li>
				<li>
					<a href="">
						Aviso de privacidad
					</a>
				</li>
			</ul>
		</footer>

	</div>


@endsection
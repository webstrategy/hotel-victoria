<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Ferias</title>

        <!-- FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Encode+Sans+Expanded:300,400,500,600" rel="stylesheet">
        <!-- BOOTSTRAP 4 -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <!-- STYLES -->
        <link rel="stylesheet" href="{{ asset('css/ferias/app.css') }}">
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('ferias/css/slick.css') }}"/>
    </head>
    <body>
    
        @yield('content')
        
        <!-- JQUERY -->
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" crossorigin="anonymous"></script>
        <!-- BOOTSTRAP JS --> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
        <!-- SLICK JS -->
        <script type="text/javascript" src="{{ asset('ferias/js/slick.js') }}"></script>
        <!-- MAP -->
        <script src="{{ asset('ferias/js/map.js') }}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1xdEVYy8IZdBKJGQp_QpDWaNQT7ZHGhY&extension=.js"></script>
        
        <script type="text/javascript">
            $(window).on('load',function(){

                $('#myModal').modal('show');

            });

            $(document).ready(function(){

                $('.btn-register').click(function(event){
                    event.preventDefault();
                    
                    if($(".my-modal").hasClass("display-block")){
                        $(".my-modal").removeClass("display-block")
                    }else{
                        $(".my-modal").addClass("display-block");
                    }
                });

                $('.close-modal').click(function(event){
                    event.preventDefault();
                    $(".my-modal").removeClass("display-block")
                });

                // MODAL PAYMENT FORMS
                $('.btn-payment').click(function(event){
                    event.preventDefault();
                    
                    if($(".my-modal-payment").hasClass("display-block")){
                        $(".my-modal-payment").removeClass("display-block")
                    }else{
                        $(".my-modal-payment").addClass("display-block");
                    }
                });
                
                //CLOSE MODAL PAYMENT
                $('.close-modal').click(function(event){
                    event.preventDefault();
                    $(".my-modal-payment").removeClass("display-block")
                });


                // MENU
                $('#menu-icon i').click(function(event){
                    event.preventDefault();

                    if($("#menu-icon i").hasClass("fas fa-bars")){
                        $("#menu-icon i").removeClass('fas fa-bars').addClass('far fa-times-circle');
                        $("body").css("overflow-y", "hidden");
                        
                        $(".vertical-menu li").addClass('menu-effect');
                        
                        $(".menu-content").css({
                            top: '0px',
                            // transition: 'all 0.5s ease-in-out',
                        });
                    }else{
                        $("#menu-icon i").removeClass('far fa-times-circle').addClass('fas fa-bars');
                        $("body").css("overflow-y", "visible");
                        $(".menu-content").css({
                            top: '-100%',
                            // transition: 'all 0.5s ease-in-out',
                        });

                        $(".vertical-menu li").removeClass('menu-effect');
                        
                    }
                });

                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                        $('.slick2').slick({
                            rows: 1,
                            arrows: true,
                            infinite: true,
                            speed: 300,
                            slidesToShow:4,
                            slidesToScroll: 2,
                            prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
                            nextArrow: '<div class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
                            responsive: [
                                {
                                    breakpoint: 1024,
                                    settings: {
                                        slidesPerRow: 1,
                                        rows: 1,
                                        slidesToScroll: 1,
                                        slidesToShow: 3,
                                        dots: false
                                    }
                                },
                                {
                                    breakpoint: 680,
                                    settings: {
                                        slidesPerRow: 1,
                                        rows: 1,
                                        slidesToScroll: 1,
                                        slidesToShow: 2,
                                        dots: false
                                    }
                                },
                                {
                                    breakpoint: 450,
                                    settings: {
                                        slidesPerRow: 1,
                                        rows: 1,
                                        slidesToScroll: 1,
                                        slidesToShow: 1,
                                        dots: false
                                    }
                                }
                            ]
                        });

                        $('#palenque-tab').click(function(){
                            $('.slick1').slick({
                                rows: 1,
                                arrows: true,
                                infinite: true,
                                speed: 300,
                                slidesToShow: 4,
                                slidesToScroll: 4,
                                prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
                                nextArrow: '<div class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
                                responsive: [
                                    {
                                    breakpoint: 1024,
                                    settings: {
                                        slidesPerRow: 1,
                                        rows: 1,
                                        slidesToScroll: 1,
                                        slidesToShow: 3,
                                        dots: false
                                    }
                                    },
                                    {
                                        breakpoint: 680,
                                        settings: {
                                            slidesPerRow: 1,
                                            rows: 1,
                                            slidesToScroll: 1,
                                            slidesToShow: 2,
                                            dots: false
                                        }
                                    },
                                    {
                                        breakpoint: 450,
                                        settings: {
                                            slidesPerRow: 1,
                                            rows: 1,
                                            slidesToScroll: 1,
                                            slidesToShow: 1,
                                            dots: false
                                        }
                                    }
                                ]
                            });
                        });
                } else {

                    $('.slick2').slick({
                        rows: 2,
                        arrows: true,
                        infinite: true,
                        speed: 300,
                        slidesToShow:4,
                        slidesToScroll: 2,
                        prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
                        nextArrow: '<div class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
                        responsive: [
                            {
                                breakpoint: 768,
                                settings: {
                                    slidesPerRow: 1,
                                    rows: 1,
                                    slidesToScroll: 1,
                                    slidesToShow: 1,
                                    dots: false
                                }
                            },
                            {
                                breakpoint: 680,
                                settings: {
                                    slidesPerRow: 1,
                                    rows: 1,
                                    slidesToScroll: 1,
                                    slidesToShow: 1,
                                    dots: false
                                }
                            },
                            {
                                breakpoint: 480,
                                settings: {
                                    rows: 1,
                                    slidesPerRow: 1,
                                    slidesToScroll: 1,
                                    slidesToShow: 1,
                                    dots: false,
                                }
                            }
                        ]
                    });

                    $('#palenque-tab').click(function(){
                        $('.slick1').slick({
                            rows: 2,
                            arrows: true,
                            infinite: true,
                            speed: 300,
                            slidesToShow: 4,
                            slidesToScroll: 4,
                            prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
                            nextArrow: '<div class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
                            responsive: [
                                {
                                    breakpoint: 768,
                                    settings: {
                                        slidesPerRow: 1,
                                        rows: 1,
                                        slidesToScroll: 1,
                                        slidesToShow: 1,
                                        dots: false
                                    }
                                },
                                {
                                    breakpoint: 480,
                                    settings: {
                                        rows: 1,
                                        slidesPerRow: 1,
                                        slidesToScroll: 1,
                                        slidesToShow: 1,
                                        dots: false
                                    }
                                }
                            ]
                        });
                    });

                } //END IF



                $('.slick3').slick({
                    arrows: true,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
                    nextArrow: '<div class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
                    responsive: [
                        {
                            breakpoint: 740,
                            settings: {
                                // slidesPerRow: 2,
                                slidesToScroll: 1,
                                slidesToShow: 2,
                                dots: false
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToScroll: 1,
                                slidesToShow: 1,
                                dots: false
                            }
                        }
                    ]
                });
            });

        </script>

        <script> 
            google.maps.event.addDomListener(window, 'load', init);
            var map;
            function init() {
            var mapOptions = {
            center: new google.maps.LatLng(18.889721,-98.972882),
            zoom: 16,
            zoomControl: true,
            zoomControlOptions: {
              style: google.maps.ZoomControlStyle.DEFAULT,
            },
            disableDoubleClickZoom: false,
            mapTypeControl: true,
            mapTypeControlOptions: {
              style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            },
            scaleControl: true,
            scrollwheel: false,
            streetViewControl: true,
            draggable : true,
            overviewMapControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [{stylers:[{saturation:-100},{gamma:1}]},{elementType:"labels.text.stroke",stylers:[{visibility:"off"}]},{featureType:"poi.business",elementType:"labels.text",stylers:[{visibility:"off"}]},{featureType:"poi.business",elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"poi.place_of_worship",elementType:"labels.text",stylers:[{visibility:"off"}]},{featureType:"poi.place_of_worship",elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"road",elementType:"geometry",stylers:[{visibility:"simplified"}]},{featureType:"water",stylers:[{visibility:"on"},{saturation:50},{gamma:0},{hue:"#50a5d1"}]},{featureType:"administrative.neighborhood",elementType:"labels.text.fill",stylers:[{color:"#333333"}]},{featureType:"road.local",elementType:"labels.text",stylers:[{weight:0.5},{color:"#333333"}]},{featureType:"transit.station",elementType:"labels.icon",stylers:[{gamma:1},{saturation:50}]}]
            }

            var mapElement = document.getElementById('map');
            var map = new google.maps.Map(mapElement, mapOptions);
            var locations = [
            ['Paraíso Cocoyito', 18.889721, -98.972882]
            ];
            for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
              icon: "{{ asset('ferias/images/pin_map.png') }}",
              position: new google.maps.LatLng(locations[i][1], locations[i][2]),
              map: map
            });
            }
            }
        </script>

    </body>
</html>

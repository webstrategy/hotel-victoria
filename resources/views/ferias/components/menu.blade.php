<div class="menu-content">

	<ul class="vertical-menu">
		<li><a href="">HOTEL VICTORIA</a></li>
		<li><a href="{{ url('/landing') }}">FERIA TEXCOCO</a></li>
		<li><a href="{{ url('/landing') }}">FERIA NUEVO LEÓN</a></li>
	</ul>

	<ul class="redes-menu">
		<li>
			<a href="">
				<i class="fab fa-twitter"></i>
			</a>
		</li>
		<li>
			<a href="">
				<i class="fab fa-facebook-f"></i>
			</a>
		</li>
		<li>
			<a href="">
				<i class="fab fa-instagram"></i>
			</a>
		</li>
		<li>
			<a href="">
				<i class="fab fa-youtube"></i>
			</a>
		</li>
	</ul>

</div>
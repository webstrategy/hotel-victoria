@extends('base')

@section('metakey', ', boletos para hotel victoria.')

@section('title', 'Hotel Victoria - Boletos')
@section('active-home', 'active')

@section('header')
	@include('partials._header')
@endsection

@section('content')
<div class="container-fluid ">

  <div class="row " style="margin-left: -15px; margin-right: -15px;">
    <div class="boletos">
			<div class="col-md-offset-1 col-md-10 hidden-sm hidden-xs txt_opacity" style="margin-top:40px">Ahora en  Hotel Victoria</div>
      <div class="col-md-offset-2 col-md-3 hidden-sm hidden-xs" align="center" style="margin-top:50px">
        <div class="experi" id="ex-mortal" style="cursor:pointer">
          <div class="txt_titulo_boletos">
            <a href="#tickets-area">Hotel de Leyendas<br><u style="text-decoration:none; border-bottom:5px solid #fff; padding-bottom:1px;">Victoria</u>&nbsp;<img src="images/hv_skull.png" alt=""></a>
          </div>
          <!-- <div class="txt_dama">
            Sweet roll jujubes carrot cake liquorice pastry. Tart jelly cotton candy caramels gummies tart wafer oat cake oat cake.
          </div> -->
        </div>
      </div>

      <div class="col-md-offset-1 col-md-3 hidden-sm hidden-xs" align="center" style="margin-top:50px;opacity:0.4;">
        <div class="experi2" id="ex-extrema" style="">
          <div class="txt_titulo_boletos">
          Hotel de Leyendas<br>Victoria <br>Acceso <u style="text-decoration:none; border-bottom:5px solid #fff; padding-bottom:1px;">Total</u> &nbsp;<img src="images/hv_fire.png" alt="">
          </div>
          <!-- <div class="txt_dama">
            Dessert carrot cake topping sugar plum tiramisu icing dessert cheesecake. Cotton candy powder gingerbread jelly-o croissant.
          </div> -->
        </div>
      </div>
			<div class="col-md-offset-1 col-md-10 hidden-sm hidden-xs txt_opacity">*EVENTO PARA MAYORES DE EDAD</div>

			<!-- MOBILE -->
      <div class="col-sm-12 col-xs-12 visible-sm visible-xs txt_opacity" style="margin-top:40px">Ahora en  Hotel Victoria</div>
      <div class="col-sm-12 col-xs-12 visible-sm visible-xs" align="center" style="margin-top:50px">
          <div class="experi_m" id="ex-mortal-m" style="cursor:pointer">
            <div class="txt_titulo_boletos_m">
              <a href="#tickets-area">Hotel de Leyendas<br><u style="text-decoration:none; border-bottom:5px solid #fff; padding-bottom:1px;">Victoria</u>&nbsp;<img src="images/hv_skull.png" alt=""></a>
            </div>
            <!-- <div class="txt_dama_m">
              Sweet roll jujubes carrot cake liquorice pastry. Tart jelly cotton candy caramels gummies tart wafer oat cake oat cake.
            </div> -->
          </div>
      </div>

      <div class="col-sm-12 col-xs-12 visible-sm visible-xs" align="center" style="opacity:0.4;">
          <div class="experi2_m" id="ex-extrema-m" style="">
            <div class="txt_titulo_boletos_m">
              Hotel de Leyendas<br>Victoria <br>-<br>Acceso <u style="text-decoration:none; border-bottom:5px solid #fff; padding-bottom:1px;">Total</u>&nbsp;<img src="images/hv_fire.png" alt="">
            </div>
            <!-- <div class="txt_dama_m">
              Dessert carrot cake topping sugar plum tiramisu icing dessert cheesecake. Cotton candy powder gingerbread jelly-o croissant.
            </div> -->
          </div>
      </div>

			<div class="col-md-offset-1 col-md-10 txt_opacity"  align="center"></div>
			<div class="col-sm-12 col-xs-12 visible-sm visible-xs txt_opacity">*EVENTO PARA MAYORES DE EDAD</div>
    </div>
   </div>

 <div class="row" style="padding-top:30px">
    <div class="col-md-offset-1 col-md-5 col-sm-12 col-xs-12">
      <div class="titulos"><u style="text-decoration: none; border-bottom: 5px solid #ea002a; padding-bottom:7px;">Pr</u>óximas fechas
    </div>
    </div>

      <div class="col-md-offset-1 col-md-4 hidden-sm hidden-xs" align="right">
				<input type="text" class="dropbtn" id="fecha-m" placeholder="fecha de evento">
				<!-- <button type="button" id="" class="btn btn-danger" title="Calendario">
					<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
				</button> -->
      </div>
      <!-- <div class="col-md-2 hidden-sm hidden-xs" align="right">
        <div class="dropdown">
          <button id="dropdown-item-hours" class="dropbtn">Todos los horarios<div class="glyphicon glyphicon-triangle-bottom flechita">
          </div> </button>
          <div id="dropdown-hours" class="dropdown-content">
            <a href="#"></a>
            <a href="#"></a>
            <a href="#"></a>
            <a href="#"></a>
          </div>
        </div>
      </div> -->
  <!-- mobile -->
      <div class="col-sm-12 col-xs-12 visible-sm visible-xs" style="padding-top:50px">
				<input type="text" class="dropbtn" id="fecha" placeholder="fecha de evento">
				<!-- <button type="button" id="" class="btn btn-danger" title="Calendario">
					<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
				</button> -->
      </div>
      <!-- <div class="col-sm-12 col-xs-12 visible-sm visible-xs">
        <div class="dropdown">
          <button id="dropdown-item-hours-m" class="dropbtn">Todos los horarios<div class="glyphicon glyphicon-triangle-bottom flechita">
          </div> </button>
          <div id="dropdown-hours-m" class="dropdown-content">
            <a href="#">C</a>
            <a href="#"></a>
            <a href="#"></a>
            <a href="#"></a>
          </div>
        </div>
      </div> -->

</div>

	<!-- FECHAS -->
	<div id="tickets-area"></div>


</div>
@endsection

@section('content-js')
<!-- <script src="js/moment_with_locale.js"></script> -->
<script src="js/datepicker-es.js"></script>
<script type="text/javascript" src="js/controllers/tickets.js"></script>
@endsection

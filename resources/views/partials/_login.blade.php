<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.10";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 registro">
                <div class="row"align="center">
                  <div class="registro_logo"><br/><img src="images/logo_victoria.png" alt=""></div>
                </div>

                <div class="row"align="center">
                    <div class="txt_titulo_registo visible-lg visible-md hidden-sm hidden-xs" style="margin-top:25px;">¿ERES MAYOR<br>DE EDAD?</div>
                    <div class="txt_titulo_registo visible-sm visible-xs hidden-lg hidden-md" style="margin-top:25px;">¿ERES MAYOR DE EDAD?</div>
                    <div class="" style="margin-left:8px; margin-right:8px">
                      <p> Para poder entrar a este sitio debes tener más de 18 años.<br/>Por favor, verifica tu edad.</p>

                      <div class="txt_opacity" style="margin-top:1px;" >
                        <a style="padding-right: 20px;" href="https://www.gmodelo.mx/terminos.html" target="_blank">Términos y condiciones</a>
                        <a href="https://www.gmodelo.mx/politicas.html" target="_blank">Políticas de privacidad</a>
                      </div>

                    </div>


                    <input id="day_field" class="caja_registro" type="text" name="DÍA" minLength="1" maxlength="2" min="1" max="31" placeholder="DÍA">
                    <input id="month_field" class="caja_registro" type="text" name="MES" maxlength="2" min="1" max="12" placeholder="MES">
                    <input id="year_field" class="caja_registro" type="text"  minLength="4" maxlength="4" name="AÑO" min="" max="" placeholder="AÑO">
                </div>

              <div class="row" align="center" style="margin-top:15px;">
                <input class="boton_registro" type="button" value="ENTRAR" id="login_btn"><br>
                <!-- <p id="fb-login" style="cursor: pointer;"><u>O INGRESA CON FACEBOOK</u></p> -->


                <img src="images/hv_boton_fb.png" width="192px" height="30px" id="fb-login" style="cursor: pointer;" />

                <!-- <div class="fb-login-button" data-max-rows="1" data-size="medium" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" id="fb-login" data-use-continue-as="false"></div>
                <p id="fb-login" style="cursor: pointer;"><u>O INGRESA CON FACEBOOK</u></p> -->
              </div>

              <div class="row" align="center" style="margin-top:50px;">
                <div class="txt_leg">TODO CON MEDIDA <br>PATROCINADO POR VICTORIA</div>
                <!-- <div class="txt_opacity" style="margin-top:30px;" >
                  <a style="padding-right: 20px;" href="https://www.gmodelo.mx/terminos.html" target="_blank">Términos y condiciones</a>
                  <a href="https://www.gmodelo.mx/politicas.html" target="_blank">Políticas de privacidad</a>
                </div> -->
              </div>
            </div>
        </div>
       </div>
    </div>
  </div>
</div>

<div id="alert-modal" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="formDialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div style="text-align: center; padding-bottom: 20px;">
        <div class="dialog-msg"></div>
        <button class="btn boton_registro" style="text-align: center;" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>

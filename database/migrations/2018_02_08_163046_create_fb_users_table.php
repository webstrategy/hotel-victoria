<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFbUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname', 200);
            $table->string('email', 255);
            $table->char('gender', 1);
            $table->string('age_range', 3);
            $table->date('birthday');
            $table->string('user_name', 255);
            $table->string('password', 255);
            $table->string('provider', 255);
            $table->string('salt', 255);
            $table->bigInteger('facebookUserId');
            $table->integer('twitterUserId');
            $table->string('twitterKey', 255);
            $table->string('twitterSecret', 255);
            $table->string('github', 255);
            $table->string('openId', 255);
            $table->string('avatar', 255);
            $table->string('roles', 255);
            $table->text('browser', 255);
            $table->text('os', 255);
            $table->text('ip', 255);
            $table->text('device', 255);
            $table->text('user_info', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fb_users');
    }
}

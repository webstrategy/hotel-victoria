<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartelerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carteleras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('feria_id')->unsigned();
            $table->string('title', 30);
            $table->string('image_cover', 255);
            $table->date('date_cartel');
            $table->string('type', 30);
            $table->timestamps();

            $table->foreign('feria_id')->references('id')->on('ferias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carteleras');
    }
}

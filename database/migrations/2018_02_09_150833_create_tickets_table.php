<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('feria_id')->unsigned();
            $table->string('title', 200);
            $table->text('description');
            $table->integer('cost');
            $table->text('dates');
            $table->integer('order');
            $table->integer('status');
            $table->timestamps();

            $table->foreign('feria_id')->references('id')->on('ferias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
